var endpoints = location.origin;
var registrationEsbnController = angular
		.module('registrationEsbnController', ['ngSanitize'])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])
						
						.directive('nonNulldrop', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    });

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

registrationEsbnController.controller('requestRegistration', function($scope,
		$http, $rootScope, $location, $window) {
	/*
	 * $scope.tester = "tester"; $scope.tester2 = {id : 12,par:"123"};
	 * 
	 * $scope.loadData = function(){  $http.get(endpoints +
	 * "/registrationWsCtl/tester",config) .then( function(response) { 
	 * $scope.tester = response.data; }, function error(response) { 
	 * $scope.postResultMessage = "Error with status: " + response.statusText;
	 * }); }
	 * 
	 * $scope.loadData();
	 */

	$scope.registasiInfo = {};
	$scope.addRekeningDanaInfo = {};
	$scope.addRekeningSurat = {};
    $scope.timerOtpRegEsbn = '';
    $scope.enableBtnRegEsbn= false;
    $scope.timerOtpAddRekDana = '';
    $scope.enableBtnAddRekDana= false;
	
     
    
	setInterval(function() {
		$http.get("registrationWsCtl/count-otp-reg-esbn").then(
		function(response){
			//debugger;
				$scope.timerOtpRegEsbn = "0:00"
				    $scope.enableBtnRegEsbn = false;
					if(response.data.valid && response.data.value > 0){
						$scope.timerOtpRegEsbn = "0:"+response.data.value;
						$scope.enableBtnRegEsbn = true;
			}

		},
		function error(response){
			
		});
		
		
	},1000);
	 
	
	setInterval(function() {
		$http.get("registrationWsCtl/count-otp-add-rekdana").then(
		function(response){
			//debugger;
				$scope.timerOtpAddRekDana = "0:00"
				    $scope.enableBtnAddRekDana = false;
					if(response.data.valid && response.data.value > 0){
						$scope.timerOtpAddRekDana = "0:"+response.data.value;
						$scope.enableBtnAddRekDana = true;
			}

		},
		function error(response){
			
		});
		
		
	},1000);
	    
    
    
	$scope.getRekeningDana = function(){
		$http.get("registrationWsCtl/getRekDana", config)
		.then(
				function(response) {
					
				   $scope.listSeriesRekDana = response.data;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	

	$scope.getRekeningDanaUnexisting = function(){
		$http.get("registrationWsCtl/getRekDana/unExisting", config)
		.then(
				function(response) {
					
				   $scope.listSeriesUnexisting = response.data;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	$scope.getRekeningDanaUnexisting();
	$scope.getRekeningDana();
	
	
	$scope.getRekeningSB = function(){
		$http.get("registrationWsCtl/getRekSB", config)
		.then(
				function(response) {
					
				   $scope.listSeries = response.data;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	$scope.getRekeningSBUnexisting = function(){
		$http.get("registrationWsCtl/getRekSB/unExisting", config)
		.then(
				function(response) {
					
				   $scope.listSeriesUnexistingSB = response.data;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}

	$scope.getRekeningSBUnexisting();
	$scope.getRekeningSB();
	
	$scope.saveMyRegistration = function(myRegistrasi) {
		//debugger;
		$scope.registasiInfo;
		if($scope.checkingRegistration){
		if($scope.registasiInfo.noRekTerpilih == undefined ){
			alert('Mohon pastikan Anda sudah memilih rekening dana');
		}else{
		// $scope.addRekeningDanaInfo
			  var r = confirm("Apakah data Anda sudah valid?");
			 // debugger;
			  if (r == true) {			 
		$http.post("registrationWsCtl/save-myRegistrationEsbn",
				myRegistrasi).then(
				function(response) {
					
						if(response.data.valid){
							
							alert(response.data.reason);
							//debugger;
							window.location = "esbn-profile";	
						}else{
							alert(response.data.reason);
						}
				},
				function error(response) {
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
			  } 		
		}
		}else{
			alert('Pastikan Anda telah membaca dan mencentang isi dari Syarat dan Ketentuan');
		}
	}
	
	$scope.saveMyNewRekeningDana = function(myRekeningDana) {
		// $scope.registasiInfo;
		$scope.addRekeningDanaInfo;
		$scope.addRekeningDanaInfo.noRekTerpilihs = $scope.sizeModelDetail;
		
		$http.post("registrationWsCtl/addnew-rekeningdana",
				myRekeningDana).then(
				function(response) {
					if(response.data.valid){
						
						alert("Berhasil menambahkan rekening dana");
						window.location = "esbn-profile";	
					}else{
						alert(response.data.reason);
					}
				},
				function error(response) {
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		 

	 
	}
	
	$scope.saveMyNewRekeningSurat = function(myRekeningSurat) {
		// $scope.registasiInfo;
		$scope.addRekeningSurat;
		
			$scope.addRekeningSurat;
			$scope.addRekeningSurat.noRekTerpilihSB = $scope.rekeningSB;
		$http.post("registrationWsCtl/addnew-rekeningsb",myRekeningSurat).then(
				function(response) {
				
				if(response.data.valid){
					
					alert("Berhasil menambahkan rekening surat berharga");
					window.location = "esbn-profile";	
				}else{
					alert(response.data.reason);
				}
				},
				function error(response) {
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});

	}

	$scope.sizeModelDetail = [ {
	} ];

	$scope.addModel = function() {
		$scope.sizeModelDetail.push({
		});
	}

	$scope.rekeningSB = [ {
	} ];

	$scope.addRekeningSB = function() {
		$scope.rekeningSB.push({
		});
	}
	
	$scope.backDashboard = function() {
		window.location = "";
	}
	
	// call otp
	$scope.callOtpInvestor = function(){
		//debugger;
		var nohp = $scope.registasiInfo.noHp;
		$http.get("otp-calling/get-otp-registrationEsbn/"+nohp)
		.then(
				function(response) {
					//debugger;
					alert(response.data.message);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	//$scope.callOtpInvestor();

	$scope.callOtpRekDana = function(){
		$http.get("otp-calling/get-otp-addRekDana")
		.then(
				function(response) {
					alert(response.data.message);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}

	$scope.callOtpRekSB = function(menu,partisi){
		var real = menu+partisi;
		$http.post("otp-calling/create-otp"+registasiInfo.noHp, real)
		.then(
				function(response) {
					alert(response.data.message);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	$scope.masterWording = function(){
		//debugger;
		$http.post("master-wording/disclaimer", 'DISC1VERS1')
		.then(
				function(response) {
					//debugger;
					$scope.disclaimer1 = response.data;
				},
				function error(response) {
					//debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		$http.post("master-wording/disclaimer", 'DISC1VERS2')
		.then(
				function(response) {
					//debugger;
					$scope.disclaimer2 = response.data;
				},
				function error(response) {
					//debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	$scope.masterWording();

	
	/*
	 * $scope.getRekDana = function() { 
	 *  
	 * $http.get(endpoints + "/esbn-registration", config) .then(
	 * function(response) {  }, function error(response) { 
	 * $scope.postResultMessage = "Error with status: " + response.statusText;
	 * }); }
	 * 
	 * $scope.getRekDana();
	 */
});