var endpoints = location.origin;
var portofolioController = angular
		.module('portofolioController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ]).directive('format', ['$filter', function ($filter) {
						    return {
						        require: '?ngModel',
						        link: function (scope, elem, attrs, ctrl) {
						            if (!ctrl) return;


						            ctrl.$formatters.unshift(function (a) {
						                return $filter(attrs.format)(ctrl.$modelValue)
						            });


						            ctrl.$parsers.unshift(function (viewValue) {
						                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
						                elem.val($filter(attrs.format)(plainNumber));
						                return plainNumber;
						            });
						        }
						    };
						}])
						.directive('nonNulldrop', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    });



var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

portofolioController.controller('portofolio', function($scope, $http,
		$rootScope, $location, $window) {
	
   $scope.kolom = {};
   $scope.kolom.type = 'all';
   $scope.kolom.produk = 'all';
   $scope.kolom.pemesanan = 'all';
   $scope.paging = {};
   
 $scope.callFilter = function(paging){
	 $scope.paging.pageNumber = paging; 
	   $http.post("portofolio/initilize-paging-filter",$scope.paging)
		.then(
				function(response) {
					//debugger;
					$scope.paging.dataTable = response.data.dataTable;
					$scope.paging.dataTableAll = response.data.dataTableAll;
					$scope.paging.sid = response.data.sid;
					$scope.paging.namaProduks = response.data.namaProduks;
					$scope.paging.pageNumber = response.data.pageNumber;
					$scope.paging.pagesize = response.data.pagesize;
					$scope.paging.totalPage = response.data.totalPage;
					$scope.paging.totalRecord = response.data.totalRecord;
				},
				function error(response) {
					//debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
 }
   
 $scope.paginationMe = function(paging){
	 //debugger;
	 $scope.callFilter(paging);
   }
   
   $scope.callMyTable = function(){
	   
	   $http.get("portofolio/initialize")
		.then(
				function(response) {
					//debugger;
					$scope.paging.dataTable = response.data.dataTable;
					$scope.paging.dataTableAll = response.data.dataTableAll;
					$scope.paging.sid = response.data.sid;
					$scope.paging.namaProduks = response.data.namaProduks;
					$scope.paging.pageNumber = response.data.pageNumber;
					//$scope.paging.produkTerpilih = response.data.namaProduks[0];
					$scope.paging.kolomTypeTerpilih = 'All';
					$scope.paging.pagesize = response.data.pagesize;
					$scope.paging.totalPage = response.data.totalPage;
					$scope.paging.totalRecord = response.data.totalRecord;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
   }
   $scope.callMyTable();
   
   

});

