var endpoints = location.origin;
var transactionController = angular
		.module('transactionController', ['ngSanitize'])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])
.directive('format', ['$filter', function ($filter) {
						    return {
						        require: '?ngModel',
						        link: function (scope, elem, attrs, ctrl) {
						            if (!ctrl) return;
						            ctrl.$formatters.unshift(function (a) {
						                return $filter(attrs.format)(ctrl.$modelValue)
						            });
						            ctrl.$parsers.unshift(function (viewValue) {
						                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
						                elem.val($filter(attrs.format)(plainNumber));
						                return plainNumber;
						            });
						        }
						    };
						}])
//validasi untuk input						
.directive('nonNull', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"color":"red","border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    })
//validasi untuk drop down
.directive('nonNulldrop', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    })	    
	    
.directive('nominalValidation', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
        control.$validators.adult = function (modelValue, viewValue) {
        	
        	var minPemesanan = parseInt(document.getElementsByName("minPemesanan")[0].value.replace(/,/g,''));
        	var maxPemesanan = parseInt(document.getElementsByName("maxPemesanan")[0].value.replace(/,/g,''));
        	var kelipatan = parseInt(document.getElementsByName("kelipatan")[0].value.replace(/,/g,''));
             
             if(modelValue==undefined){
            	 
             document.getElementsByName("validationNominal")[0].value = "Pastikan Anda mengisi nominal transaksi pemesanan " ;
             $(element).css({"color":"red","border-color" : "red"})
             }
             else if(isNaN(minPemesanan) && isNaN(maxPemesanan)){
            	 
               document.getElementsByName("validationNominal")[0].value = "Pastikan Anda memilih nama Produk untuk transaksi pemesanan. " ;
               $(element).css({"color":"red","border-color" : "red"})
             }
             else if(parseInt(modelValue)>maxPemesanan )
             {
            	 
            	 document.getElementsByName("validationNominal")[0].value = "Nominal pemesanan melebihi kuota investor " ;
            	 $(element).css({"color":"red","border-color" : "red"})	 
             }
             else if(parseInt(modelValue)<minPemesanan){
            	 
            	 document.getElementsByName("validationNominal")[0].value = "Minimal pemesanan sebesar " + document.getElementsByName("minPemesanan")[0].value;
            	 $(element).css({"color":"red","border-color" : "red"})	 
             }
             else if(modelValue%parseInt(kelipatan)!=0){
             $(element).css({"color":"red","border-color" : "red"})
              document.getElementsByName("validationNominal")[0].value = "Nominal pemesanan harus kelipatan " + document.getElementsByName("kelipatan")[0].value ;
             }
             else{
             $(element).css({"color":"black","border-color" : "green"}) 
             return true;
             }
        };
    }
};
})
.directive('nominalValidationEarly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, control) {
        control.$validators.adult = function (modelValue, viewValue) {
        	
        	var minPemesanan = parseInt(document.getElementsByName("minPemesanan")[0].value.replace(/,/g,''));
        	//var maxPemesanan = parseInt(document.getElementsByName("maxPemesanan")[0].value.replace(/,/g,''));
        	var kelipatan = parseInt(document.getElementsByName("kelipatan")[0].value.replace(/,/g,''));
            var redeemable =  parseInt(document.getElementsByName("redeemable")[0].value.replace(/,/g,''));
             if(modelValue==undefined){
            	 document.getElementsByName("validationNominal")[0].value = "Pastikan Anda mengisi nominal pencairan" ;
             $(element).css({"color":"red","border-color" : "red"})
             }
//             else if(isNaN(minPemesanan)){
//             $(element).css({"color":"red","border-color" : "red"})
//             }
             else if(parseInt(modelValue)<minPemesanan){
             $(element).css({"color":"red","border-color" : "red"})	 
              document.getElementsByName("validationNominal")[0].value = "Minimal pencairan sebesar " + document.getElementsByName("minPemesanan")[0].value;
             }
             else if(parseInt(modelValue)>redeemable){
             $(element).css({"color":"red","border-color" : "red"})	
               document.getElementsByName("validationNominal")[0].value = "Pencairan tidak boleh lebih besar dari sisa yang bisa dicairkan";
             }
             else if(modelValue%parseInt(kelipatan)!=0){
             $(element).css({"color":"red","border-color" : "red"})
              document.getElementsByName("validationNominal")[0].value = "Nominal pencairan harus kelipatan " + document.getElementsByName("kelipatan")[0].value;
             }
             else{
             $(element).css({"color":"black","border-color" : "green"}) 
             return true;
             }
        };
    }
};
});

transactionController.$inject = ['$scope'];

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

transactionController.controller('transaksiPemesanan', function($scope, $http,
		$rootScope, $location, $window) {
	
	$scope.enableTncDiv = true;
	
	$scope.enableTnc = function(){
		$scope.enableTncDiv = false;
	}
	
	setInterval(function() {
		$http.get("TransaksiPemesananWsCtl/count-otp").then(
				function(response) {
					// 
					$scope.myTimer = "0:00"
					$scope.enableButton = false;
					if (response.data.valid
							&& response.data.value > 0) {
						$scope.myTimer = "0:"
								+ response.data.value;
						$scope.enableButton = true;
					}
				}, function error(response) {

				});

	}, 1000);
	
	$scope.masterWording = function(){
	
		$http.post("master-wording/disclaimer/map", 'DISC2VERS1')
		.then(
				function(response) {
					
					$scope.disclaimer1 = response.data.words;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
		$http.post("master-wording/guide/map", 'GUID1VERS1')
		.then(
				function(response) {
					
					$scope.disclaimer2 = response.data.words;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		$http.post("master-wording/guide/map", 'GUID2VERS1')
		.then(
				function(response) {
					
					$scope.disclaimer3 = response.data.words;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		$http.post("master-wording/guide/map", 'GUID3VERS1')
		.then(
				function(response) {
					
					$scope.disclaimer4 = response.data.words;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	
	$scope.callOtp = function(menu,partisi){
		
		var real = menu+partisi;
		$http.get("otp-calling/get-otp-transaksiPemesanan")
		.then(
				function(response) {
					
					alert(response.data.message);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}

	$scope.nextTabs = function(idNumber,next) {
	
		if($scope.checkingDesclaimer){
		if($scope.infoUser.seriTerpilih == undefined){
			alert('Pastikan Anda telah memilih nama produk yang diinginkan');
		}
			else if(
				$scope.infoUser.nominal == undefined || 
				$scope.infoUser.rekeningSBTerpilih == undefined){
			var validationWording = document.getElementsByName("validationNominal")[0].value;
			alert(validationWording);
		}
		else if($scope.infoUser.rekeningDanaTerpilih == undefined){
			alert('Pastikan Anda telah memilih No Rekening Dana');
		}
		else{
		angular.element(document.getElementsByClassName("nav-item "+idNumber+"  active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("nav-link "+idNumber+" active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+idNumber+" active"))
				.removeClass('active');

		angular.element(document.getElementsByClassName("nav-item "+next))
				.addClass('active');
		angular.element(document.getElementsByClassName("nav-link "+next))
				.addClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+next+" fade"))
				.removeClass('fade');
		angular.element(document.getElementsByClassName("tab-pane "+next))
				.addClass('active');
		}
		}else{
			alert('pastikan Anda telah membaca dan mencentang isi dari memorandum informasi');
		}
	}
	
	$scope.beforeTabs = function(idNumber,last) {

		angular.element(document.getElementsByClassName("nav-item "+idNumber+"  active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("nav-link "+idNumber+" active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+idNumber+" active"))
				.removeClass('active');

		angular.element(document.getElementsByClassName("nav-item "+last))
				.addClass('active');
		angular.element(document.getElementsByClassName("nav-link "+last))
				.addClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+last+" fade"))
				.removeClass('fade');
		angular.element(document.getElementsByClassName("tab-pane "+last))
				.addClass('active');

		
	}
	
	$scope.transactionPemesanan = {};
	$scope.infoUser = {};
	$scope.getSeries = function(){

		$scope.masterWording();
		$http.get("TransaksiPemesananWsCtl/initialize", config)
		.then(
				function(response) {
					
				   $scope.listSeries = response.data.produks;
				   $scope.infoUser = response.data.info;
				   $scope.infoUser.subRegno = $scope.infoUser.rekeningSB[0].namaSubregistry;
				   $scope.infoUser.rekeningSBTerpilih = $scope.infoUser.rekeningSB[0];
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	$scope.getSeries();
	
	$scope.selectedSeri = function(){
		$scope.infoUser;
		$http.post("TransaksiPemesananWsCtl/getKuotaSeriBySidAndId",
				$scope.infoUser.seriTerpilih.id)
		.then(
				function(response) {
					
					if(response.data.KuotaInvestor==undefined){
						 $scope.infoUser.sisaKuotaPemesanan = '';
					}else{
				   $scope.infoUser.sisaKuotaPemesanan = response.data.KuotaInvestor;
					}
					
					if(response.data.KuotaSeri==undefined){
				   $scope.infoUser.sisaKuotaSeri = '';
					}else{
						  $scope.infoUser.sisaKuotaSeri = response.data.KuotaSeri;
					}
					
					if(response.data.startSettlemen==undefined){
						 $scope.infoUser.startSettlemen = '';
					}else{
				   $scope.infoUser.startSettlemen = response.data.startSettlemen;
					}
				},
				function error(response) {
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}
	
	$scope.itsLoading = {
			validModal : false,
			invalidModal : false,
			waitModal : true
	};
	
	$scope.saveTransaction = function(){
		$scope.itsLoading.waitModal = true;
		$scope.infoUser;
		$scope.itsLoading.invalidModal = false;
		$scope.hashTransaction = {};
		$scope.hashTransaction.idSeri = $scope.infoUser.seriTerpilih.id;
		$scope.hashTransaction.idRekDana = $scope.infoUser.rekeningDanaTerpilih.id;
		$scope.hashTransaction.idRekSB = $scope.infoUser.rekeningSBTerpilih.id;
		$scope.hashTransaction.nominal = $scope.infoUser.nominal;
		$scope.hashTransaction.myOtp = $scope.infoUser.myOtp;
		$scope.hashTransaction ;
		
		$http.post("TransaksiPemesananWsCtl/saveTransaksionalPemesanan",
				$scope.hashTransaction)
		.then(
				function(response) {
				    
				    if(response.data.valid){
				    $scope.nextTabs(2,3);
				    $scope.reason = response.data.reason;
				    $scope.resultSave = response.data;
				    $scope.itsLoading.validModal = true;
				    $scope.itsLoading.invalidModal = false;
				    $scope.itsLoading.waitModal = false;
					}
					else{
						$scope.reason = response.data.reason;
						$scope.itsLoading.invalidModal = true;
						 $scope.itsLoading.validModal = false;
					    $scope.itsLoading.waitModal = false;
				    }
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}
	
});


transactionController.controller('transaksiEarlyRedemption', function($scope, $http,
		$rootScope, $location, $window) {
	
	setInterval(function() {
		$http.get("TransaksiEarlyredemptionWsCtl/count-otp").then(
				function(response) {
				
					$scope.myTimer = "0:00"
					$scope.enableButton = false;
					if (response.data.valid
							&& response.data.value > 0) {
						$scope.myTimer = "0:"
								+ response.data.value;
						$scope.enableButton = true;
					}
				}, function error(response) {

				});

	}, 1000);
	
	$scope.masterWording = function(){
		
		$http.post("master-wording/disclaimer/map", 'DISC3VERS1')
		.then(
				function(response) {
					
					$scope.disclaimer1 = response.data.words;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}

	$scope.masterWording();
	$scope.callOtp = function(menu,partisi){
		//var real = menu+partisi;
		$http.get("otp-calling/get-otp-transaksiEarlyRedemption")
		.then(
				function(response) {
					
					alert(response.data.message);
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}

	
	$scope.nextTabs = function(idNumber,next) {
		$scope.infoUser;
		if($scope.checkingDesclaimer){
			if($scope.infoUser.seriTerpilih == undefined){
				alert('Pastikan Anda telah memilih nama produk yang diinginkan');
			}
			else if($scope.infoUser.transaksiTerpilih == undefined){
				alert('Pastikan Anda telah memilih kode pemesanan');
			}
				else if(
					$scope.infoUser.nominal == undefined || 
					$scope.infoUser.rekeningSBTerpilih == undefined){
				var validationWording = document.getElementsByName("validationNominal")[0].value;
				alert(validationWording);
			}
			else{
		angular.element(document.getElementsByClassName("nav-item "+idNumber+"  active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("nav-link "+idNumber+" active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+idNumber+" active"))
				.removeClass('active');

		angular.element(document.getElementsByClassName("nav-item "+next))
				.addClass('active');
		angular.element(document.getElementsByClassName("nav-link "+next))
				.addClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+next+" fade"))
				.removeClass('fade');
		angular.element(document.getElementsByClassName("tab-pane "+next))
				.addClass('active');
		}
		
	}else{
		alert('pastikan Anda telah membaca dan mencentang isi dari memorandum informasi');
	}
	}
	
	$scope.beforeTabs = function(idNumber,last) {

		angular.element(document.getElementsByClassName("nav-item "+idNumber+"  active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("nav-link "+idNumber+" active"))
				.removeClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+idNumber+" active"))
				.removeClass('active');

		angular.element(document.getElementsByClassName("nav-item "+last))
				.addClass('active');
		angular.element(document.getElementsByClassName("nav-link "+last))
				.addClass('active');
		angular.element(document.getElementsByClassName("tab-pane "+last+" fade"))
				.removeClass('fade');
		angular.element(document.getElementsByClassName("tab-pane "+last))
				.addClass('active');

		
	}
	
	$scope.transactionPemesanan = {};
	$scope.infoUser = {};
	$scope.getSeries = function(){
		$http.get("TransaksiEarlyredemptionWsCtl/initialize", config)
		.then(
				function(response) {
					
				   $scope.listSeries = response.data.produks;
				   $scope.infoUser = response.data.info;
				   $scope.infoUser.subRegno = $scope.infoUser.rekeningSB[0].namaSubregistry;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
	}
	
	$scope.getSeries();
	
	$scope.selectedSeri = function(){
		$scope.infoUser;
		$http.post("TransaksiEarlyredemptionWsCtl/getKuotaSeriBySidAndId",
				$scope.infoUser.seriTerpilih)
		.then(
				function(response) {
					
				   $scope.infoUser.transaksis = response.data;
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}
	
	$scope.selectedPemesanan = function(){
		
		$scope.infoUser.rekeningSBTerpilih = $scope.infoUser.transaksiTerpilih.esbnPemesananTransaksi.rekeningSB;
		$scope.infoUser.rekeningDanaTerpilih = $scope.infoUser.transaksiTerpilih.esbnPemesananTransaksi.rekeningDana;
		
		
		
	}
	
	$scope.itsLoading = {
			validModal : false,
			invalidModal : false,
			waitModal : true
	};
	
	$scope.saveTransaction = function(){
		
	    $scope.itsLoading.waitModal = true;
	    $scope.itsLoading.invalidModal = false;
		$scope.infoUser;
		$scope.hashTransaction = {};
		$scope.hashTransaction.kodePemesanan = $scope.infoUser.transaksiTerpilih.esbnPemesananTransaksi.kodePemesanan;
		$scope.hashTransaction.nominal = $scope.infoUser.nominal;
		$scope.hashTransaction.myOtp = $scope.infoUser.myOtp;
		$scope.hashTransaction.rekeningDana = $scope.infoUser.rekeningDanaTerpilih.noRek;
		$scope.hashTransaction ;
		
		
		
		$http.post("TransaksiEarlyredemptionWsCtl/saveTransaksionalPemesanan",
				$scope.hashTransaction)
		.then(
				function(response) {
					if(response.data.valid){
				    
				    $scope.nextTabs(2,3);
				    $scope.resultSave = response.data;
				    $scope.reason = response.data.reason;
				    $scope.itsLoading.validModal = true;
				    $scope.itsLoading.invalidModal = false;
				    $scope.itsLoading.waitModal = false;
					}
					else{
						$scope.reason = response.data.reason;
						$scope.itsLoading.invalidModal = true;
						$scope.itsLoading.validModal = false;
					    $scope.itsLoading.waitModal = false;
					}
				},
				function error(response) {
					
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
				});
		
	}
	
});
