package com.bank.mega.controller.master;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;

@RestController
@RequestMapping("/master-wording")
public class MasterWordingWs extends BaseService{

	@GetMapping("/login/error-wording-searching")
	public String wordingLoginErrorHandler(HttpServletRequest request,HttpSession session ) {
		String wording = (String) session.getAttribute("reason-error");
		if(wording==null||wording.equals("")) {
			return "Password atau No Sid yang Anda masukkan masih salah";
		}
		return wording;
	}
	
	@PostMapping("/disclaimer")
	public String wordingDisclaimer(
			@RequestBody String idType){
		HttpRestResponse httpRestResponse =
				wsBody(wmsParentalWrapper+"/shared/master/disclaimer/DiscAndGuide/"+idType+"/id",
						null, HttpMethod.GET, null);
		return httpRestResponse.getBody();
	}
	
	@PostMapping("/disclaimer/map")
	public Map<String, Object> wordingDisclaimer2(
			@RequestBody String idType){
		Map<String, Object> mapper = new HashMap<>();
		HttpRestResponse httpRestResponse =
				wsBody(wmsParentalWrapper+"/shared/master/disclaimer/DiscAndGuide/"+idType+"/id",
						null, HttpMethod.GET, null);
		mapper.put("words", httpRestResponse.getBody());
		return mapper;
	}
	
	@PostMapping("/guide/map")
	public Map<String, Object> guideWording1(
			@RequestBody String idType){
		Map<String, Object> mapper = new HashMap<>();
		HttpRestResponse httpRestResponse =
				wsBody(wmsParentalWrapper+"/shared/master/guide/DiscAndGuide/"+idType+"/id",
						null, HttpMethod.GET, null);
		mapper.put("words", httpRestResponse.getBody());
		return mapper;
	}
}
