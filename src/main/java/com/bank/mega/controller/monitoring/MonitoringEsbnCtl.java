package com.bank.mega.controller.monitoring;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.EsbnMonitoringEarlyRedemption;
import com.bank.mega.service.BaseService;

@Controller
public class MonitoringEsbnCtl extends BaseService {

	@RequestMapping("/esbn-monitoring-pemesanan")
	public String esbnMonitoringPemesanan(Model model, Authentication auth, HttpServletRequest request,
			HttpServletResponse response, EsbnMonitoringEarlyRedemption esbnMonitoringEarlyRedemption) {
		// model.addAttribute("beanRegistry", getWmsValueBySid(auth.getName()));
		// EsbnMonitoringPemesanan esbnMonitoringPemesanan = new
		// EsbnMonitoringPemesanan();
		// esbnMonitoringPemesanan.setSid(auth.getName());
		// model.addAttribute("beanMonitoringPemesanan", esbnMonitoringPemesanan);
		informationHeader(model, auth);

		if (isMenuAccessable(auth)) {
			return checkSessionValidity("monitoring/monitoring-pemesanan-esbn", auth, request, response);
		} else {
			return checkSessionValidity("monitoring/noreg/monitoring-pemesanan-esbn-noreg", auth, request, response);
		}
	}

	@RequestMapping("/esbn-monitoring-earlyRedemption")
	public String esbnMonitoringEarlyRedemption(Model model, Authentication auth, HttpServletRequest request,
			HttpServletResponse response) {
		// model.addAttribute("beanRegistry", getWmsValueBySid(auth.getName()));
		/*
		 * EsbnMonitoringEarlyRedemption earlyRedemption = new
		 * EsbnMonitoringEarlyRedemption(); earlyRedemption.setSid(auth.getName());
		 * model.addAttribute("beanEarlyRedemption", earlyRedemption);
		 */
		informationHeader(model, auth);
		if (isMenuAccessable(auth)) {
			return checkSessionValidity("monitoring/monitoring-earlyredemption-esbn", auth, request, response);
		} else {
			return checkSessionValidity("monitoring/noreg/monitoring-earlyredemption-esbn-noreg", auth, request, response);
		}
	}

}
