package com.bank.mega.controller.monitoring;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.EsbnAllSeriProductEarlyRedemption;
import com.bank.mega.bean.EsbnPemesananEarlyRedemption;
import com.bank.mega.bean.EsbnPemesananTransaksi;
import com.bank.mega.bean.EsbnPortofolioDataBeanV2;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.bean.portofolio.EsbnPemesananEarlyRedemptionComparable;
import com.bank.mega.bean.portofolio.EsbnPemesananTransaksiComparable;
import com.bank.mega.bean.portofolio.PortofolioDataTableBean;
import com.bank.mega.bean.portofolio.PortofolioPagingReader;
import com.bank.mega.bean.principal.BasePrincipalSessions;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/portofolio")
public class PortofilioEsbnWsCtl extends BaseService {

	@PostMapping("/initilize-paging-filter")
	public PortofolioPagingReader getAllPagingPortofolio(@RequestBody PortofolioPagingReader portofolioPagingReader) {

		int i = 1;
		List<String> filterProduk = new ArrayList<>();
		List<String> filterType = new ArrayList<>();

		if (portofolioPagingReader.getProdukTerpilih().equals("All")) {
			filterProduk.addAll(portofolioPagingReader.getNamaProduks());
		} else {
			filterProduk.add(portofolioPagingReader.getProdukTerpilih());
		}

		if (portofolioPagingReader.getKolomTypeTerpilih().equals("All")) {
			filterType.add("Transaksi Pemesanan");
			filterType.add("Transaksi Early Redemption");
		} else {
			filterType.add(portofolioPagingReader.getKolomTypeTerpilih());
		}

		List<EsbnPortofolioDataBeanV2> dataPagingDummy = new ArrayList<>();

		for (EsbnPortofolioDataBeanV2 bean : portofolioPagingReader.getDataTableAll()) {
			if (insensitiveFilter(bean.getNamaProduk(), filterProduk)) {
				dataPagingDummy.add(bean);
			}
		}

		List<EsbnPortofolioDataBeanV2> dataTableBeans = new ArrayList<>();
		for (EsbnPortofolioDataBeanV2 paging : dataPagingDummy) {

			if (i >= (int) (((portofolioPagingReader.getPageNumber() - 1) * 10) + 1)
					&& i <= portofolioPagingReader.getPageNumber() * 10) {
				dataTableBeans.add(paging);
			}
			i++;
		}

		portofolioPagingReader.setDataTable(dataTableBeans);
		portofolioPagingReader.setTotalPage((long) Math.ceil(dataPagingDummy.size() / 10.0));
		return portofolioPagingReader;
	}

	private boolean insensitiveFilter(String words, List<String> linked) {
		for (String lin : linked) {
			if (lin.equals(words))
				return true;
		}
		return false;
	}

	@GetMapping("/initialize")
	public Map<String, Object> initializeDataPortofoli(Authentication authentication) {
		Map<String, Object> mapp = new HashMap<>();
		Set<String> namaProduks = new LinkedHashSet<>();
		TokenWrapper tokenWrapper = getMyToken(authentication);
		HttpRestResponse allTrxPemesanan = wsBodyEsbnWithoutBody("/v1/pemesanan/seri/all/" + authentication.getName(),
				"GET", null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		List<EsbnPemesananTransaksi> esbnPemesananTransaksis = new ArrayList<>();
		List<PortofolioDataTableBean> dataTableBeans = new ArrayList<>();
		try {
			esbnPemesananTransaksis = mapperJsonToListDto(allTrxPemesanan.getBody(), EsbnPemesananTransaksi.class);
			List<EsbnPemesananTransaksiComparable> esbnPemesananTransaksiComparables = new ArrayList<>();
			for (EsbnPemesananTransaksi trx : esbnPemesananTransaksis) {
				EsbnPemesananTransaksiComparable comparable = new EsbnPemesananTransaksiComparable();
				comparable = mapperFacade.map(trx, EsbnPemesananTransaksiComparable.class);
				comparable.setTglJatuhTempo(parseDateStringToAnotherStringFormat(trx.getTglJatuhTempo(),
						"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
				comparable.setComparableDateTransaksi(parseDateStringToAnotherStringFormat(trx.getTglPemesanan(),
						"yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyyMMddHHmmss"));
				esbnPemesananTransaksiComparables.add(comparable);
			}

			if (esbnPemesananTransaksis.size() > 0) {
				namaProduks.add("All");
			}

			EsbnPemesananTransaksiComparable[] comparables = esbnPemesananTransaksiComparables
					.toArray(new EsbnPemesananTransaksiComparable[esbnPemesananTransaksiComparables.size()]);
			Arrays.sort(comparables, EsbnPemesananTransaksiComparable.DATE_TRANSAKSI);

			for (EsbnPemesananTransaksiComparable trx : Arrays.asList(comparables)) {

				if (trx.getIdStatus().equals(ESBN_IDSTATUS_COMPLETE)) {

					namaProduks.add(trx.getSeri());
					HttpRestResponse allRedeem = wsBodyEsbnWithoutBody(
							"/v1/redemption/seri/" + trx.getIdSeri() + "/" + authentication.getName(), "GET", null,
							tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

					HttpRestResponse seriRedeemOffer = wsBodyEsbnWithoutBody("/v1/seri/redeem/offer", "GET", null,
							tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

					List<EsbnAllSeriProductEarlyRedemption> allSeriProductEarlyRedemptions = mapperJsonToListDto(
							seriRedeemOffer.getBody(), EsbnAllSeriProductEarlyRedemption.class);
					BigInteger bitMaxPcent = new BigInteger("0");
					for (EsbnAllSeriProductEarlyRedemption comp : allSeriProductEarlyRedemptions) {
						if (comp.getId() == comp.getId()) {
							System.err.println("masuk kesini nggak");
							bitMaxPcent = comp.getMaxPcent();
							break;
						}
					}

					List<EsbnPemesananEarlyRedemption> earlyRedemptions = new ArrayList<>();
					try {
						earlyRedemptions = mapperJsonToListDto(allRedeem.getBody(), EsbnPemesananEarlyRedemption.class);
					} catch (Exception e) {
						System.err.println("error kenape : " + allRedeem.getBody());
					}
					PortofolioDataTableBean dataTableBeanTrx = new PortofolioDataTableBean();
					dataTableBeanTrx.setTanggalJatuhTempo(trx.getTglJatuhTempo());
					dataTableBeanTrx.setKodePemesanan(trx.getKodePemesanan());
					dataTableBeanTrx.setNamaProduk(trx.getSeri());
					dataTableBeanTrx.setNominal(trx.getNominal());
					dataTableBeanTrx.setSisaNominal(trx.getNominal());
					dataTableBeanTrx.setTanggalSettelment(parseDateStringToAnotherStringFormat(trx.getTglSetelmen(),
							"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
					BigInteger sisaRedeem = (trx.getNominal().multiply(bitMaxPcent)).divide(new BigInteger("100"));
					dataTableBeanTrx.setSisaYangDapatDiredeem(sisaRedeem);
					dataTableBeanTrx.setComparableDateTransaksi(trx.getComparableDateTransaksi());
					dataTableBeanTrx.setTanggalTransaksi(parseDateStringToAnotherStringFormat(trx.getTglPemesanan(),
							"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy")

					);

					dataTableBeanTrx.setTipePemesanan("Transaksi Pemesanan");
					dataTableBeans.add(dataTableBeanTrx);
					if (earlyRedemptions != null) {
						BigInteger bitEarly = new BigInteger("0");
						List<EsbnPemesananEarlyRedemptionComparable> comparableEarlies = new ArrayList<>();

						for (EsbnPemesananEarlyRedemption comp : earlyRedemptions) {
							if (comp.getTglRedeem() != null) {
								EsbnPemesananEarlyRedemptionComparable comparable = new EsbnPemesananEarlyRedemptionComparable();
								comparable = mapperFacade.map(comp, EsbnPemesananEarlyRedemptionComparable.class);
								comparable.setComparableDateTransaksi(parseDateStringToAnotherStringFormat(
										comp.getTglRedeem(), "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyyMMddHHmmss"));
								comparableEarlies.add(comparable);
							}
						}

						EsbnPemesananEarlyRedemptionComparable[] earlyComp = comparableEarlies
								.toArray(new EsbnPemesananEarlyRedemptionComparable[comparableEarlies.size()]);
						Arrays.sort(earlyComp, EsbnPemesananEarlyRedemptionComparable.DATE_TRANSAKSI);

						for (EsbnPemesananEarlyRedemptionComparable early : Arrays.asList(earlyComp)) {
							System.err.println("early gw : " + new Gson().toJson(early));
							if (early.getNominal() != null) {
								if (early.getKodePemesanan().equalsIgnoreCase(trx.getKodePemesanan())) {
									bitEarly = bitEarly.add(early.getNominal());

									PortofolioDataTableBean dataTableBean = new PortofolioDataTableBean();
									dataTableBean.setTanggalJatuhTempo(trx.getTglJatuhTempo());
									dataTableBean.setKodePemesanan(early.getKodePemesanan());
									dataTableBean.setKodeReedeem(early.getKodeRedeem());
									dataTableBean.setNamaProduk(trx.getSeri());
									dataTableBean.setNominal(early.getNominal());
									dataTableBean.setSisaNominal(trx.getNominal().subtract(bitEarly));
									// System.err.println("setlement : " + early.getTglSetelmen());
									dataTableBean.setTanggalSettelment(parseDateStringToAnotherStringFormat(
											early.getTglSetelmen(), "yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
									dataTableBean.setSisaYangDapatDiredeem(sisaRedeem.subtract(bitEarly));
									dataTableBean.setTanggalTransaksi(parseDateStringToAnotherStringFormat(
											early.getTglRedeem(), "yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
									dataTableBean.setTipePemesanan("Transaksi Early Redemption");
									dataTableBean.setComparableDateTransaksi(early.getComparableDateTransaksi());
									dataTableBeans.add(dataTableBean);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.err.println("semua data out :  " + new Gson().toJson(dataTableBeans));

		PortofolioDataTableBean[] earlyBean = dataTableBeans
				.toArray(new PortofolioDataTableBean[dataTableBeans.size()]);
		Arrays.sort(earlyBean, PortofolioDataTableBean.DATE_TRANSAKSI);

		dataTableBeans.clear();
		dataTableBeans = Arrays.asList(earlyBean);

		List<EsbnPortofolioDataBeanV2> dataTableBeansAllNew = new ArrayList<>();

		for (String produk : namaProduks) {

			if (!produk.equalsIgnoreCase("ALL")) {
				BigInteger nominalPemesananAwal = new BigInteger("0");
				BigInteger nominalYangSudahDiredeem = new BigInteger("0");
				BigInteger sisaKepemilikanTerakhir = new BigInteger("0");
				String tanggalJatuhTempo = "";
				for (PortofolioDataTableBean dtb : dataTableBeans) {
					if (dtb.getNamaProduk().equals(produk)) {
						if (dtb.getTipePemesanan().equals("Transaksi Pemesanan")) {
							nominalPemesananAwal = nominalPemesananAwal.add(dtb.getNominal());
						} else if (dtb.getTipePemesanan().equals("Transaksi Early Redemption")) {
							nominalYangSudahDiredeem = nominalYangSudahDiredeem.add(dtb.getNominal());
						}
						System.err.println("dtb nya : " + new Gson().toJson(dtb));
						if (dtb.getTanggalJatuhTempo() != null) {
							tanggalJatuhTempo = dtb.getTanggalJatuhTempo();
						}
					}
				}

				EsbnPortofolioDataBeanV2 pdtb = new EsbnPortofolioDataBeanV2();
				pdtb.setNamaProduk(produk);
				pdtb.setNominalPemesananAwal(nominalPemesananAwal);
				pdtb.setNominalYangSudahDiredeem(nominalYangSudahDiredeem);
				pdtb.setTanggalJatuhTempo(tanggalJatuhTempo);
				sisaKepemilikanTerakhir = nominalPemesananAwal.subtract(nominalYangSudahDiredeem);
				pdtb.setSisaKepemilikanTerakhir(sisaKepemilikanTerakhir);
				dataTableBeansAllNew.add(pdtb);
			}
		}

		int i = 1;
		List<EsbnPortofolioDataBeanV2> pagingAllNew = new ArrayList<>();
		for (EsbnPortofolioDataBeanV2 bean : dataTableBeansAllNew) {
			if (i < 11) {
				pagingAllNew.add(bean);
			} else {
				break;
			}
			i++;
		}
		pagingAllNew = new ArrayList<>();
		// Set<PortofolioNamaProduk> setProduks = new HashSet<>(namaProduks);
		mapp.put("sid", authentication.getName());
		mapp.put("dataTable", pagingAllNew);
		mapp.put("dataTableAll", dataTableBeansAllNew);
		mapp.put("namaProduks", namaProduks);
		mapp.put("totalPage", Math.ceil(pagingAllNew.size() / 10.0));
		mapp.put("pagesize", 10);
		mapp.put("pageNumber", 1);
		mapp.put("totalRecord", dataTableBeans.size());
		return mapp;
	}

}
