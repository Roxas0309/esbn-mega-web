package com.bank.mega.controller.transaction;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.ErrorHttpHandler;
import com.bank.mega.bean.EsbnAllSeriProductEarlyRedemption;
import com.bank.mega.bean.EsbnInfoUser;
import com.bank.mega.bean.EsbnMetamorphosisKodePemesan;
import com.bank.mega.bean.EsbnPemesananEarlyRedemption;
import com.bank.mega.bean.EsbnPemesananTransaksi;
import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.SaveMyTransactionEarly;
import com.bank.mega.bean.SaveMyTransactionEarlyRedemption;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/TransaksiEarlyredemptionWsCtl")
public class TransaksiEarlyredemptionWsCtl extends BaseService{
	private String redeemResult(Authentication authentication) {
		TokenWrapper tokenWrapper =  getMyToken(authentication);
		return tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token();
	}
	
	@GetMapping("/count-otp")
	public Map<String, Object> countOtp(HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		if (session.getAttribute(OTP_TIMER_EARLY) != null) {
			int counter = (int) session.getAttribute(OTP_TIMER_EARLY) - 1;
			session.setAttribute(OTP_TIMER_EARLY, counter);
			map.put("valid", true);
			map.put("value", counter);
		}

		return map;
	}
	
	@GetMapping("/initialize")
	public Map<String, Object>getAllSeriesProduct(Authentication authentication){	
		Map<String, Object> map = new HashMap<>();
		List<EsbnAllSeriProductEarlyRedemption> allSeriProductEarlyRedemptions = new ArrayList<>();
		EsbnInfoUser esbnInfoUser = new EsbnInfoUser();
		TokenWrapper tokenWrapper =  getMyToken(authentication);
		HttpRestResponse responseSeri = wsBodyEsbnWithoutBody
				("/v1/seri/redeem/offer", "GET", null, tokenWrapper.getToken_type() + " " + 
		tokenWrapper.getAccess_token());
		HttpRestResponse responseInfo = wsBodyEsbnWithoutBody
				("/v1/investor/"+authentication.getName(), "GET", null, tokenWrapper.getToken_type() + " " + 
		tokenWrapper.getAccess_token());
		System.out.println(new Gson().toJson(responseSeri));
		try {	
			allSeriProductEarlyRedemptions = mapperJsonToListDto(responseSeri.getBody(), 
					EsbnAllSeriProductEarlyRedemption.class);
			esbnInfoUser = mapperJsonToSingleDto(responseInfo.getBody(), 
					EsbnInfoUser.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<EsbnAllSeriProductEarlyRedemption> seriProductEarlyRedemptions = new ArrayList<>();
		for (EsbnAllSeriProductEarlyRedemption earlyProduct : allSeriProductEarlyRedemptions) {
			EsbnAllSeriProductEarlyRedemption redemption = new EsbnAllSeriProductEarlyRedemption();
			redemption = earlyProduct;
			
			
			HttpRestResponse existingTrx = wsBodyEsbnWithoutBody("/v1/pemesanan/seri/"+redemption.getId()+"/"+authentication.getName(), "GET", null,
					tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			try {
				List<EsbnAllSeriProductEarlyRedemption> transaksis = mapperJsonToListDto(existingTrx.getBody(), EsbnAllSeriProductEarlyRedemption.class);
				System.err.println("transaksi early : " + new Gson().toJson(transaksis));
				List<EsbnAllSeriProductEarlyRedemption> earlyRedemptions = new ArrayList<>(transaksis);
				for (EsbnAllSeriProductEarlyRedemption early : earlyRedemptions) {
					if(early.getSeri()==null) {
						transaksis.remove(early);
					}
				}
				if(transaksis!=null&&transaksis.size()>0) {
					redemption.setPeriodeRedemption(parseDateStringToAnotherStringFormat(earlyProduct.getTglMulaiRedeem(),
							"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy")+" - " + parseDateStringToAnotherStringFormat
							(earlyProduct.getTglAkhirRedeem(),
									"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy HH:mm:ss"));
					redemption.setTglSetelmen(parseDateStringToAnotherStringFormat(earlyProduct.getTglSetelmen(),
							"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
					seriProductEarlyRedemptions.add(redemption);
				}
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		map.put("produks", seriProductEarlyRedemptions);
		map.put("info", esbnInfoUser);
		return map;
	}
	
	@PostMapping("/getKuotaSeriBySidAndId")
	public List<EsbnMetamorphosisKodePemesan> resultKuota(@RequestBody EsbnAllSeriProductEarlyRedemption idSeri, Authentication authorization){
		
		// coba pake ini nanti https://base_url/v1/pemesanan/seri/{IdSeri}/{Sid}/{IdStatus}
		HttpRestResponse responseKuota = wsBodyEsbnWithoutBody
				("/v1/pemesanan/seri/"+idSeri.getId()+"/"+authorization.getName(), 
						"GET", null, redeemResult(authorization));
		List<EsbnPemesananTransaksi> esbnPemesananTransaksis = new ArrayList<>();
		try {
			esbnPemesananTransaksis = mapperJsonToListDto
					(responseKuota.getBody(), EsbnPemesananTransaksi.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<EsbnMetamorphosisKodePemesan> maps = new ArrayList<>();
		
		for (EsbnPemesananTransaksi map : esbnPemesananTransaksis) {
			EsbnMetamorphosisKodePemesan pemesan = new EsbnMetamorphosisKodePemesan();
			pemesan.setEsbnPemesananTransaksi(map);
			System.err.println("sisa : " + map.getSisaKepemilikan() + " max pcent : " +idSeri.getMaxPcent() );
			BigInteger bi = (map.getNominal().multiply(idSeri.getMaxPcent())).divide(new BigInteger("100"));
			BigInteger minimalRedemable = bi.subtract(map.getRedeem());
			pemesan.setMaximumPemesananRedeem(bi);
			pemesan.setRedeemable(minimalRedemable);
			pemesan.setTransaksiNominal(map.getKodePemesanan() + " - Rp. " + changeFormatNumberMoney(map.getNominal()));
			maps.add(pemesan);
		}
		
		return maps;
	}
	
	@PostMapping("/saveTransaksionalPemesanan")
	public Map<String, Object> resultTransactionPemesanan(@RequestBody SaveMyTransactionEarlyRedemption myTrx
			,HttpSession session, Authentication authorization){
		
		if(callOtp(session, "transaksiEarlyRedemption", "")==null||callOtp(session, "transaksiEarlyRedemption", "").equals("")) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Mohon Minta Kode OTP Terlebih Dahulu");
			return errorMap;
		}
		
		int timerOtp = (int) session.getAttribute(OTP_TIMER_EARLY);
		int wrongOtpAnswer = (int) session.getAttribute(OTP_WRONG_ANSWER_EARLY);
		long timerOtpStart = (long) session.getAttribute(OTP_START_TIME_EARLY);
		Float porcessCallWs = (System.currentTimeMillis() - timerOtpStart) / 1000F;

		System.out.println("otp wrong : " + new Gson().toJson(wrongOtpAnswer));
		System.out.println("otp timer : " + new Gson().toJson(porcessCallWs));

		if (timerOtp <= 0 || porcessCallWs >= 60) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Kode OTP yang anda minta sudah kadaluarsa. Mohon minta kembali kode OTP anda");
			return errorMap;
		}

		if (wrongOtpAnswer <= 0) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Anda sudah melakukan 3 kali kesalahan. Mohon minta kembali kode OTP anda");
			session.removeAttribute(OTP_WRONG_ANSWER_EARLY);
			session.removeAttribute(OTP_START_TIME_EARLY);
			session.removeAttribute(OTP_TIMER_EARLY);
			session.removeAttribute("transaksiEarlyRedemption");
			return errorMap;
		}
		
		if(myTrx.getMyOtp()==null) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Mohon isi otp terlebih dahulu");
			return errorMap;
		}
		
		if(!myTrx.getMyOtp().equalsIgnoreCase(callOtp(session, "transaksiEarlyRedemption", "")))
		{
			session.setAttribute(OTP_WRONG_ANSWER_EARLY, wrongOtpAnswer - 1);
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Otp yang dimasukkan belum valid");
			return errorMap;
		}
		
		session.removeAttribute(OTP_WRONG_ANSWER_EARLY);
		session.removeAttribute(OTP_START_TIME_EARLY);
		session.removeAttribute(OTP_TIMER_EARLY);
		session.removeAttribute("transaksiEarlyRedemption");
		
		HttpRestResponse myTransaksiPemesanan = wsBodyEsbnWithoutBody
				("/v1/pemesanan/"+myTrx.getKodePemesanan(), 
						"GET", null, redeemResult(authorization));
		
	    try {
			EsbnPemesananTransaksi transaksi = mapperJsonToSingleDto(myTransaksiPemesanan.getBody(),
					EsbnPemesananTransaksi.class);
			System.err.println("transaksi : " + new Gson().toJson(transaksi));
			System.err.println("myTrx : " + new Gson().toJson(myTrx));
			if(transaksi==null||transaksi.getSid()==null) {
				Map<String, Object> errorMap = new HashMap<>();
				errorMap.put("valid", false);
				errorMap.put("reason", "Transaksi Tidak Ditemukan");
				return errorMap;
			}
			if(!transaksi.getSid().equalsIgnoreCase(authorization.getName())) {
				Map<String, Object> errorMap = new HashMap<>();
				errorMap.put("valid", false);
				errorMap.put("reason", "Transaksi Yang Dipilih Bukan Punya Anda");
				return errorMap;
			}
			if(!transaksi.getRekeningDana().getNoRek().equalsIgnoreCase(myTrx.getRekeningDana())) {
				Map<String, Object> errorMap = new HashMap<>();
				errorMap.put("valid", false);
				errorMap.put("reason", "Rekening Yang Dipilih Bukan Rekening Dana Untuk Early Redemption Transaksi " + myTrx.getKodePemesanan());
				return errorMap;
			}
			
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	    
		String auth = redeemResult(authorization);
		myTrx.setSid(authorization.getName());

		System.err.println(new Gson().toJson(myTrx));
		SaveMyTransactionEarly early = mapperFacade.map(myTrx, SaveMyTransactionEarly.class);
		early.setSid(authorization.getName());
		HttpRestResponse responseTransaksiEsbn = wsBodyEsbnWithBody
				("/v1/redemption", 
						"POST", early, null, auth);
		System.out.println(new Gson().toJson(responseTransaksiEsbn.getBody()));
		
		try {
			ErrorHttpHandler handler = mapperJsonToSingleDto(responseTransaksiEsbn.getBody(),
					ErrorHttpHandler.class);
			if(handler.getCode()!=0) {
				Map<String, Object> errorMap = new HashMap<>();
				errorMap.put("valid", false);
				errorMap.put("reason","Pencairan tidak dapat dilakukan karena : " + handler.getMessage());
				return errorMap;
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		EsbnPemesananEarlyRedemption esbnPemesananEarlyRedemption = new EsbnPemesananEarlyRedemption();
		try {
			esbnPemesananEarlyRedemption = mapperJsonToSingleDto
					(responseTransaksiEsbn.getBody(), EsbnPemesananEarlyRedemption.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, Object> mapp = new HashMap<>();
		mapp = mapperJsonToHashMap(responseTransaksiEsbn.getBody());
		HttpRestResponse responseTransactionWms = wsBodyWms
				("/transactionsvc.svc/InsertRedeem", "POST", mapp, null, auth);
		System.out.println("result : " + responseTransaksiEsbn.getBody());
		String reason = "Dana Akan Dikreditkan Ke Rekening Anda Pada Tanggal <b>" + parseDateStringToAnotherStringFormat(esbnPemesananEarlyRedemption.getTglSetelmen(),
				"yyyy-MM-dd'T'HH:mm:ss'Z'", "d MMM yyyy")+ " </b>";
		mapp.put("valid", true);
		mapp.put("SisaKepemilikan", changeFormatNumberMoney( esbnPemesananEarlyRedemption.getSisaKepemilikan()));
		mapp.put("Redeemable", changeFormatNumberMoney(esbnPemesananEarlyRedemption.getRedeemable()));
		mapp.put("reason", reason);
		//Map<String, Object> info = WmsInfo(authorization);
		ProfileInvestor investor = new ProfileInvestor();
		try {
			investor = getInvestorBySidNew(authorization);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sendTransaksiEarlyRedemptionBilling(investor.getEmail(), investor.getNama(), 
				esbnPemesananEarlyRedemption,myTrx.getRekeningDana(),reason);
		return mapp;
	}
}
