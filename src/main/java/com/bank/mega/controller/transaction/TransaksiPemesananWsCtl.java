package com.bank.mega.controller.transaction;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.ErrorHttpHandler;
import com.bank.mega.bean.EsbnAllSeriProductPemesanan;
import com.bank.mega.bean.EsbnInfoUser;
import com.bank.mega.bean.EsbnPemesananTransaksi;
import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.SaveMyTransactionEsbn;
import com.bank.mega.bean.SaveMyTransactionPemesananBean;
import com.bank.mega.bean.TblEsbnPemesananTransaksiBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@RestController
@RequestMapping("/TransaksiPemesananWsCtl")
public class TransaksiPemesananWsCtl extends BaseService{

	private final static String ALL_SERI_OFFER = "allSeriOffer";
	
	@GetMapping("/count-otp")
	public Map<String, Object> countOtp(HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		if (session.getAttribute(OTP_TIMER_PEMESANAN) != null) {
			int counter = (int) session.getAttribute(OTP_TIMER_PEMESANAN) - 1;
			session.setAttribute(OTP_TIMER_PEMESANAN, counter);
			map.put("valid", true);
			map.put("value", counter);
		}

		return map;
	}
	
	private String redeemResult(Authentication authentication) {
		TokenWrapper tokenWrapper =  getMyToken(authentication);
		return tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token();
	}
	
	@GetMapping("/initialize")
	public Map<String, Object>getAllSeriesProduct(Authentication authentication, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		
		List<EsbnAllSeriProductPemesanan> esbnAllSeriProducts = new ArrayList<>();
		EsbnInfoUser esbnInfoUser = new EsbnInfoUser();
		TokenWrapper tokenWrapper =  getMyToken(authentication);
		HttpRestResponse responseSeri = wsBodyEsbnWithoutBody
				("/v1/seri/offer", "GET", null, tokenWrapper.getToken_type() + " " + 
		tokenWrapper.getAccess_token());
		HttpRestResponse responseInfo = wsBodyEsbnWithoutBody
				("/v1/investor/"+authentication.getName(), "GET", null, tokenWrapper.getToken_type() + " " + 
		tokenWrapper.getAccess_token());
		System.out.println(new Gson().toJson(responseSeri));
		try {	
			esbnAllSeriProducts = mapperJsonToListDto(responseSeri.getBody(), 
					EsbnAllSeriProductPemesanan.class);
			esbnInfoUser = mapperJsonToSingleDto(responseInfo.getBody(), 
					EsbnInfoUser.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<EsbnAllSeriProductPemesanan> esbnAllSeriProductPemesanansbaru = new ArrayList<>();
		List<String> allSeri = new ArrayList<>();
		for (EsbnAllSeriProductPemesanan seri : esbnAllSeriProducts) {
			allSeri.add(seri.getId()+"");
			session.setAttribute(ALL_SERI_OFFER, allSeri);
		    seri.setTglMulaiPemesanan(parseDateStringToAnotherStringFormat(seri.getTglMulaiPemesanan(),
					"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
		    seri.setTglAkhirPemesanan(parseDateStringToAnotherStringFormat(seri.getTglAkhirPemesanan(),
					"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy HH:mm:ss"));
		    seri.setTglJatuhTempo(parseDateStringToAnotherStringFormat(seri.getTglJatuhTempo(),
					"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
		    esbnAllSeriProductPemesanansbaru.add(seri);
		}
		
		System.err.println("info : " + new Gson().toJson(esbnInfoUser));
		
		map.put("produks", esbnAllSeriProductPemesanansbaru);
		map.put("info", esbnInfoUser);
		return map;
	}
	
	@PostMapping("/getKuotaSeriBySidAndId")
	public Map<String, Object> resultKuota(
			@RequestBody String idSeri, 
			Authentication authorization,HttpSession session){
	//	System.err.println("request :  " + new Gson().toJson(request.getRequestURL()));
		if(session.getAttribute(ALL_SERI_OFFER)==null) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "Web Service Tidak Dapat Di Bypass Tanpa Masuk Ke Menu Terlebih Dahulu");
		    return map;
		}
   
		@SuppressWarnings("unchecked")
		List<String> allSeriOffer = (List<String>) session.getAttribute(ALL_SERI_OFFER);
		
		if(!allSeriOffer.contains(idSeri)) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "Id Seri Yang Diminta Bukan Id Seri Offer");
		    return map;
		}
		
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> mapRedeem = new HashMap<>();
	
		HttpRestResponse responseKuota = wsBodyEsbnWithoutBody
				("/v1/Kuota/"+idSeri+"/"+authorization.getName(), 
						"GET", null, redeemResult(authorization));
		HttpRestResponse responseRedeem = wsBodyEsbnWithoutBody
				("/v1/seri/redeem/"+idSeri, 
						"GET", null, redeemResult(authorization));
		map = mapperJsonToHashMap(responseKuota.getBody());
		mapRedeem = mapperJsonToHashMap(responseRedeem.getBody());
		
		if(mapRedeem.get("TglMulaiRedeem")!=null&&mapRedeem.get("TglAkhirRedeem")!=null) {
		map.put("startSettlemen", parseDateStringToAnotherStringFormat((String)mapRedeem.get("TglMulaiRedeem"),
				"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy")+" - "
				+ parseDateStringToAnotherStringFormat((String)mapRedeem.get("TglAkhirRedeem"),
				"yyyy-MM-dd'T'HH:mm:ss'Z'", "dd/MM/yyyy"));
		}
		return map;
	}
	
	@PostMapping("/saveTransaksionalPemesanan")
	public Map<String, Object> resultTransactionPemesanan(@RequestBody SaveMyTransactionPemesananBean myTrx, 
			Authentication authorization,HttpSession session){
	//	System.err.println(new Gson().toJson(callOtp(session, "transaksiPemesanan", "")));
		
		if(callOtp(session, "transaksiPemesanan", "")==null||callOtp(session, "transaksiPemesanan", "").equals("")) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Mohon Minta Kode OTP Terlebih Dahulu");
			return errorMap;
		}
		
		int timerOtp = (int) session.getAttribute(OTP_TIMER_PEMESANAN);
		int wrongOtpAnswer = (int) session.getAttribute(OTP_WRONG_ANSWER_PEMESANAN);
		long timerOtpStart = (long) session.getAttribute(OTP_START_TIME_PEMESANAN);
		Float porcessCallWs = (System.currentTimeMillis() - timerOtpStart) / 1000F;

		System.out.println("otp wrong : " + new Gson().toJson(wrongOtpAnswer));
		System.out.println("otp timer : " + new Gson().toJson(porcessCallWs));

		if (timerOtp <= 0 || porcessCallWs >= 60) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Kode OTP yang anda minta sudah kadaluarsa. Mohon minta kembali kode OTP anda");
			return errorMap;
		}

		if (wrongOtpAnswer <= 0) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Anda sudah melakukan 3 kali kesalahan. Mohon minta kembali kode OTP anda");
			session.removeAttribute(OTP_WRONG_ANSWER_PEMESANAN);
			session.removeAttribute(OTP_START_TIME_PEMESANAN);
			session.removeAttribute(OTP_TIMER_PEMESANAN);
			session.removeAttribute("transaksiPemesanan");
			return errorMap;
		}
		
		if(myTrx.getMyOtp()==null) {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Mohon isi otp terlebih dahulu");
			return errorMap;
		}
		
		if(!myTrx.getMyOtp().equalsIgnoreCase(callOtp(session, "transaksiPemesanan", "")))
		{
			session.setAttribute(OTP_WRONG_ANSWER_PEMESANAN, wrongOtpAnswer - 1);
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Otp yang dimasukkan belum valid");
			return errorMap;
		}
		
		session.removeAttribute(OTP_WRONG_ANSWER_PEMESANAN);
		session.removeAttribute(OTP_START_TIME_PEMESANAN);
		session.removeAttribute(OTP_TIMER_PEMESANAN);
		session.removeAttribute("transaksiPemesanan");
		
		String auth = redeemResult(authorization);
		myTrx.setSid(authorization.getName());
	
		System.err.println(new Gson().toJson(myTrx));
		SaveMyTransactionEsbn esbnTrx = mapperFacade.map(myTrx, SaveMyTransactionEsbn.class);
		HttpRestResponse responseTransaksiEsbn = wsBodyEsbnWithBody
				("/v1/pemesanan", 
						"POST", esbnTrx, null, auth);
		System.out.println(new Gson().toJson(responseTransaksiEsbn.getBody()));
		Map<String, Object> mal = mapperJsonToHashMap(responseTransaksiEsbn.getBody());
		
		
		try {
			ErrorHttpHandler handler = mapperJsonToSingleDto(responseTransaksiEsbn.getBody(),
					ErrorHttpHandler.class);
			if(handler.getCode()!=0) {
				Map<String, Object> errorMap = new HashMap<>();
				errorMap.put("valid", false);
				errorMap.put("reason", "Pemesanan tidak dapat dilakukan karena : " + handler.getMessage());
				return errorMap;
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		EsbnPemesananTransaksi esbnPemesananTransaksi = new EsbnPemesananTransaksi();
		try {
			esbnPemesananTransaksi = mapperJsonToSingleDto
					(responseTransaksiEsbn.getBody(), EsbnPemesananTransaksi.class);
		} catch (Exception e) {
			System.err.println("masuk kesini tidak yah?");
			e.printStackTrace();
		}
		
		if(esbnPemesananTransaksi!= null && esbnPemesananTransaksi.getKodePemesanan()!=null) {
			
			
			Map<String, String> headerMap = new HashMap<>();
			headerMap.put("Authorization", redeemResult(authorization));
			TblEsbnPemesananTransaksiBean bean = new TblEsbnPemesananTransaksiBean();
			bean.setTransaksiPemesanan(esbnPemesananTransaksi.getKodePemesanan());
			bean.setTransaksiCode(esbnPemesananTransaksi.getIdStatus());
			bean.setTransaksiStatus(esbnPemesananTransaksi.getStatus());
			@SuppressWarnings("unused")
			HttpRestResponse responseSaveTransaksi = wsBody
					(wmsParentalWrapper+"/transaksi/save/transaksi-log", bean, 
							HttpMethod.POST, headerMap);
		     
		}else {
			Map<String, Object> errorMap = new HashMap<>();
			errorMap.put("valid", false);
			errorMap.put("reason", "Pemesanan tidak dapat dilakukan karena terjadi kesalahan pada internal kami");
			return errorMap;
		}
		
		
		HttpRestResponse responseTransactionWms = wsBodyWms
				("/transactionsvc.svc/InsertOrder", "POST", mal, null, auth);
		String parseTime = parseDateStringToAnotherStringFormat(esbnPemesananTransaksi.getBatasWaktuBayar(),
				"yyyy-MM-dd'T'HH:mm:ss'Z'", "d MMM yyyy, HH:mm");
		String NominalInMoney = changeFormatNumberMoney(esbnPemesananTransaksi.getNominal());
		String maintainParseTime = parseTime.replace(",", " pukul ");
		mal.put("valid", true);
		mal.put("BatasWaktuBayar",parseTime );
		mal.put("Nominal", NominalInMoney);
		mal.put("reason","Pemesanan SBN berhasil. Silakan melakukan pembayaran sebelum <b>"
		+maintainParseTime+"</b>. Jika tidak dilakukan pembayaran, maka pemesanan secara otomatis batal/menjadi kadaluarsa dan kuota akan kembali setelah 2 hari kerja.");
		System.out.println("result : " + new Gson().toJson(responseTransactionWms));
		
		esbnPemesananTransaksi.setBatasWaktuBayar(parseTime);
	
		ProfileInvestor investor = new ProfileInvestor();
		try {
			investor = getInvestorBySidNew(authorization);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sendTransaksiPemesananBilling(investor.getEmail(),investor.getNama(), esbnPemesananTransaksi);
		   
		
	
		return mal;
	}
	
}
