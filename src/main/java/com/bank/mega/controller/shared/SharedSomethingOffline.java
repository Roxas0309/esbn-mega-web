package com.bank.mega.controller.shared;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SharedSomethingOffline {
    
	@RequestMapping("/faq")
	public String faqDong() {
		return "template-faq";
	}
}
