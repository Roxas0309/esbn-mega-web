package com.bank.mega.controller.registration;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.UserBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.encryptor.SecurityData;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.WsResponse;
import com.bank.mega.service.BaseService;
import com.bank.mega.service.registration.RegistrationSvc;
import com.bank.mega.validator.EmailValidator;
import com.bank.mega.validator.PasswordValidator;
import com.bank.mega.validator.PhoneValidator;
import com.google.gson.Gson;

@RestController
@RequestMapping("/registration")
public class RegistrationWebWsCtl extends BaseService {

	@Autowired
	private RegistrationSvc registrationSvc;

	@GetMapping("/count-otp")
	public Map<String, Object> countOtp(HttpSession session) {
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		if (session.getAttribute(OTP_TIMER) != null) {
			int counter = (int) session.getAttribute(OTP_TIMER) - 1;
			session.setAttribute(OTP_TIMER, counter);
			map.put("valid", true);
			map.put("value", counter);
		}

		return map;
	}

	@PostMapping("/see-identityCard")
	public Map<String, Object> getUserByHisIdentityCard(@RequestBody UserBean userBean, HttpSession session) {
		System.err.println(new Gson().toJson(userBean));

		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		map.put("reason", "Mohon maaf data dengan SID " + userBean.getSid() + " tidak tersedia di data WMS kami");

		if (userBean.getSid() == null) {
			map.put("reason", "Mohon isi Sid Terlebih dahulu");
			return map;
		} else if (userBean.getDateBirth() == null) {
			map.put("reason", "mohon isi tanggal lahir terlebih dahulu");
			return map;
		}

		TokenWrapper tokenWrapper = getMyToken(ADDITION_USER, ADDITION_PASS, "visitor");
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("secret-user", userBean.getSid());
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		WsResponse response = getResultWs(wmsParentalWrapper + "/registeration/get-user", null, HttpMethod.GET,
				headerMap);
		TblUserEsbn tblUserDto = new TblUserEsbn();
		try {
			tblUserDto = mapperJsonToSingleDto(response.getWsContent(), TblUserEsbn.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (tblUserDto != null) {
			map.put("reason", "Mohon maaf data  " + userBean.getSid() + " telah terdaftar ");
			return map;
		}

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + userBean.getSid(), "GET", null, null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		try {
			Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
			System.err.println("customer : --> " + setObjectToString(mapp));
			System.err.println("customer : --> " + setObjectToString(mapp.get("Result")));
			Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));
			if (EmailValidator.isValid((String) mappp.get("Email"))
					&& PhoneValidator.isValid((String) mappp.get("NoHandphone"))) {

				if (mappp.get("TanggalLahir") != null) {
					String date = parseDateStringToAnotherStringFormat((String) mappp.get("TanggalLahir"), "yyyy-mm-dd",
							"dd/mm/yyyy");
					if (!userBean.getDateBirth().equals(date)) {
						map.put("reason",
								"Mohon maaf SID dan Tanggal Lahir yang dimasukkan salah. Mohon melakukan penginputan kembali.");
						return map;
					}
					// System.err.println("tanggal lahir " + date);
				}

				userBean.setEktp((String) mappp.get("NoKTP"));
				userBean.setEmail((String) mappp.get("Email"));
				userBean.setUserName((String) mappp.get("Nama"));
				map.put("valid", true);
				map.put("userBean", userBean);
			} else {
				map.put("reason", "Mohon maaf data dengan SID " + userBean.getSid()
						+ " tidak memiliki no handphone/email atau format no handphone serta email belum benar. "
						+ "Silahkan datang ke Cabang Bank Mega terdekat untuk memperbaiki Nomor handphone dan email Anda");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String enStream = null;
		try {
			enStream = new SecurityData().encrypt(new Gson().toJson(userBean));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setAttribute(USER_WILL_REGIST, enStream);

		return map;
	}

	@PostMapping("/update-password")
	public Map<String, Object> updatePassword(@RequestBody Map<String, String> mappo, Authentication authentication) {
		Map<String, Object> map = new HashMap<>();
		if (mappo == null) {
			map.put("valid", false);
			map.put("reason", "Data Masih Kosong");
			return map;
		}
		String newPass = mappo.get("newPass");
		String confirmPass = mappo.get("confirmPass");
		String tokenOtp = mappo.get("tokenOtp");
		String realTokenOtp = mappo.get("realTokenOtp");
		String realSid = mappo.get("needSid");

		boolean valid = false;
		String reason = "DONE";

		if (newPass == null || confirmPass == null || tokenOtp == null) {
			reason = "Mohon Lengkapi Form Perubahan password Anda.";
		} else if (!PasswordValidator.isValid(newPass)) {
			reason = "Mohon maaf password tidak dapat digunakan "
					+ " pastikan password anda mengandung setidaknya satu huruf besar, satu huruf kecil, "
					+ " satu karakter numerik, dan satu spesial karakter '!@#$%^&+=.,/?*()_-|<>' ";
		} else if (!tokenOtp.equals(realTokenOtp)) {
			reason = "OTP masih belum tepat";
		}

		else if (!confirmPass.equals(newPass)) {
			reason = "Password yang anda masukkan masih belum sama dengan konfirmasi yang diberikan";
		}

		else {
			Map<String, String> headerMap = new HashMap<>();
			TokenWrapper tokenWrapper = getMyToken(ADDITION_USER, ADDITION_PASS, "visitor");
			headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			headerMap.put("secret-field", "password");
			headerMap.put("secret-sid", realSid);
			Map<String, Object> body = new HashMap<>();
			body.put("password", newPass);
			@SuppressWarnings("unused")
			WsResponse response = getResultWs(wmsParentalWrapper + "/registeration/update-data-user", body,
					HttpMethod.PUT, headerMap);
			reason = "data berhasil diupdate";
			valid = true;
		}
		map.put("valid", valid);
		map.put("reason", reason);

		return map;
	}

	@PostMapping("/update-password/no-forgot")
	public Map<String, Object> updatePasswordWhenNoForgot(@RequestBody Map<String, String> mappo,
			Authentication authentication) {
		String newPass = mappo.get("newPass");
		String confirmPass = mappo.get("confirmPass");
		String oldPass = mappo.get("oldPass");
		Map<String, Object> map = new HashMap<>();
		boolean valid = false;
		String reason = "DONE";

		if (newPass == null || confirmPass == null || oldPass == null) {
			reason = "Mohon Lengkapi Data Terlebih Dahulu";
		} else if (!PasswordValidator.isValid(newPass)) {
			reason = "Mohon maaf password tidak dapat digunakan "
					+ " pastikan password anda mengandung setidaknya satu huruf besar, satu huruf kecil, "
					+ " satu karakter numerik, dan satu spesial karakter '!@#$%^&+=.,/?*()_-|<>' ";
		} else if (!getCredentialPrincipal(authentication).getPassword().equals(oldPass)) {
			reason = "Mohon maaf password lama yang anda masukkan salah";
		} else if (!confirmPass.equals(newPass)) {
			reason = "Mohon maaf konfirmasi password tidak sesuai";
		} else {
			Map<String, String> headerMap = new HashMap<>();
			TokenWrapper tokenWrapper = getMyToken(authentication);
			headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
			headerMap.put("secret-field", "password");
			headerMap.put("secret-sid", authentication.getName());
			Map<String, Object> body = new HashMap<>();
			body.put("password", newPass);
			@SuppressWarnings("unused")
			WsResponse response = getResultWs(wmsParentalWrapper + "/registeration/update-data-user", body,
					HttpMethod.PUT, headerMap);
			valid = true;
			reason = "Data Berhasil Terupdate";
		}

		map.put("valid", valid);
		map.put("reason", reason);

		return map;
	}

	@PostMapping("/send-my-otp-please")
	public Map<String, Object> getTheOtp(HttpSession session, @RequestBody String ektp) {
		System.err.println("session : " + new Gson().toJson(session.getAttribute(USER_WILL_REGIST)));
		String encBean = (String) session.getAttribute(USER_WILL_REGIST);
		UserBean bean = new UserBean();
		try {
			String dencBean = new SecurityData().decrypt(encBean);
			bean = mapperJsonToSingleDto(dencBean, UserBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (bean == null || bean.getEmail() == null) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "Web Service Ini Hanya Digunakan Pada Awal Pendaftaran Saja");
			return map;
		}

		System.err.println("Bean yang dipanggil : " + new Gson().toJson(bean));
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);
		String random = randomString(8);
		bean.setEktp(ektp);
		if (sendConfirmationUserEmail(bean.getEmail(), bean.getUserName(), bean.getSid(), random)) {
			map.put("valid", true);
			map.put("userBean", bean);
		}

		try {
			session.setAttribute(USER_WILL_REGIST, new SecurityData().encrypt(new Gson().toJson(bean)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// bean.setOtpVerification(random);
		session.setAttribute(OTP_CHECKER, random);
		session.setAttribute(OTP_WRONG_ANSWER, 3);
		session.setAttribute(OTP_TIMER, 60);
		Long porcessStartCallWs = System.currentTimeMillis();
		session.setAttribute(OTP_START_TIME, porcessStartCallWs);
		return map;
	}

	@PostMapping("/otp-forgot")
	public Map<String, Object> getTheOtpForgot(@RequestBody String userBean, Authentication au) {
		System.err.println("userbeannya " + userBean);
		Map<String, Object> map = new HashMap<>();
		map.put("valid", false);

		if (userBean == null) {
			map.put("reason", "Mohon isi SID terlebih dahulu");
			return map;
		}

		TokenWrapper tokenWrapper = getMyToken(ADDITION_USER, ADDITION_PASS, "visitor");
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("secret-user", userBean);
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		WsResponse response = getResultWs(wmsParentalWrapper + "/registeration/get-user", null, HttpMethod.GET,
				headerMap);
		TblUserEsbn tblUserDto = new TblUserEsbn();
		try {
			tblUserDto = mapperJsonToSingleDto(response.getWsContent(), TblUserEsbn.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (tblUserDto == null) {
			map.put("reason", "Mohon maaf data  " + userBean + " tidak ditemukan ");
			return map;
		}

		System.err.println(new Gson().toJson(userBean));

		String userEmail = tblUserDto.getUserEmail();
		String userName = tblUserDto.getUserName();
		String userSid = tblUserDto.getUserPhone();

		try {
			ProfileInvestor investor = getInvestorBySidNewInLogin(userBean);

			if (investor != null && investor.getEmail() != null && !investor.getEmail().equalsIgnoreCase("")) {
				userEmail = investor.getEmail();
				userName = investor.getNama();
				userSid = investor.getSid();
				System.err.println("kirim email dari esbn");
			} else {
				System.err.println("kirim email dari wms");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String random = randomString(8);
		if (sendConfirmationUserEmail(userEmail, userName, userSid, random)) {
			UserBean bean = new UserBean();
			bean.setOtpVerification(random);
			bean.setEmail(tblUserDto.getUserEmail());
			map.put("valid", true);
			map.put("userBean", bean);
		}

		return map;
	}

	@PostMapping("/registered-sid")
	public Map<String, Object> registerSidToEsbnWebNew(@RequestBody UserBean userBean, HttpSession session) {
		String encBean = (String) session.getAttribute(USER_WILL_REGIST);
		String otpVerification = (String) session.getAttribute(OTP_CHECKER);
		int otpTimer = (int) session.getAttribute(OTP_TIMER);
		int otpWrongAnswer = (int) session.getAttribute(OTP_WRONG_ANSWER);
		long otpTimerFinish = (long) session.getAttribute(OTP_START_TIME);
		Float porcessCallWs = (System.currentTimeMillis() - otpTimerFinish) / 1000F;

		UserBean bean = new UserBean();
		try {
			String dencBean = new SecurityData().decrypt(encBean);
			bean = mapperJsonToSingleDto(dencBean, UserBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("real user bean : " + new Gson().toJson(bean));
		System.err.println("user bean result : " + new Gson().toJson(userBean));
		System.err.println("otp verification : " + new Gson().toJson(otpVerification));
		System.err.println("otp wrong : " + new Gson().toJson(otpWrongAnswer));
		System.err.println("otp timer : " + new Gson().toJson(porcessCallWs));
		Map<String, Object> map = new HashMap<>();

		if (otpTimer <= 0 || porcessCallWs >= 60) {
			map.put("valid", false);
			map.put("reason",
					"Kode Verfikasi yang anda minta sudah kadaluarsa. " + " Mohon minta kembali kode verifikasi anda");
			return map;
		}

		if (otpWrongAnswer <= 0) {
			map.put("valid", false);
			map.put("reason", "Anda sudah melakukan 3 kali kesalahan. " + " Mohon minta kembali kode verifikasi anda");
			return map;
		}

		try {
			if (!PasswordValidator.isValid(userBean.getNewPass())) {
				String reason = "Mohon maaf password tidak dapat digunakan "
						+ " pastikan password anda mengandung setidaknya satu huruf besar, satu huruf kecil, "
						+ " satu karakter numerik, dan satu spesial karakter '!@#$%^&+=.,/?*()_-|<>' ";
				map.put("valid", false);
				map.put("reason", reason);
				return map;
			} else if (userBean.getOtpChecker() == null) {
				map.put("valid", false);
				map.put("reason", "Mohon isi terlebih dahulu Kode Verifikasi");
				return map;
			} else if (otpVerification == null || otpVerification.equals("")) {
				map.put("valid", false);
				map.put("reason", "Mohon minta terlebih dahulu Kode Verifikasi");
				return map;
			} else if (userBean.getOtpChecker().equals(otpVerification)
					&& userBean.getNewPass().equals(userBean.getConfirmPass())) {
				TokenWrapper tokenWrapper = getMyToken(ADDITION_USER, ADDITION_PASS, "visitor");
				HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + userBean.getSid(), "GET", null,
						null, tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
				try {
					Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
					System.err.println("customer : --> " + setObjectToString(mapp));
					System.err.println("customer : --> " + setObjectToString(mapp.get("Result")));
					Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));
					userBean.setUserPhone((String) mappp.get("NoHandphone"));
					userBean.setSid(userBean.getSid().toUpperCase());
					userBean.setOtpVerification(otpVerification);
					userBean.setEktp(bean.getEktp());
					userBean.setDateBirth(bean.getDateBirth());
					userBean.setEmail(bean.getEmail());
					userBean.setUserName(bean.getUserName());
					// userBean.setUserName(userName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.err.println("userbean : " + new Gson().toJson(userBean));
				String result = registrationSvc.resultSufficientAddNewUserEsbnCustom(userBean);

				if (result.equals(SUFFICIENT_SAVE)) {
					map.put("valid", true);
					map.put("reason", "");
					sendEmailRegistrationFirst(userBean.getEmail(), userBean.getUserName(), userBean.getSid(),
							userBean.getOtpVerification(), userBean.getNewPass());
					
					session.setAttribute(USER_WILL_REGIST, null);
					session.setAttribute(OTP_CHECKER, null);
					session.setAttribute(OTP_TIMER, null);
					session.setAttribute(OTP_WRONG_ANSWER, null);
					session.setAttribute(OTP_START_TIME, null);
					return map;
				} else {
					map.put("valid", false);
					map.put("reason", result);
					return map;
				}
			} else if (!userBean.getOtpChecker().equals(otpVerification)) {
				map.put("valid", false);
				map.put("reason", "Kode Verifikasi belum tepat");
				session.setAttribute(OTP_WRONG_ANSWER, otpWrongAnswer - 1);
				return map;
			} else if (!userBean.getNewPass().equals(userBean.getConfirmPass())) {
				map.put("valid", false);
				map.put("reason", "Password yang anda masukkan masih belum sama dengan konfirmasi yang diberikan");
				return map;
			}
		} catch (NullPointerException e) {
			map.put("valid", false);
			map.put("reason", "Mohon maaf terjadi kesalahan internal pada service kami I.");
			return map;
		}

		map.put("valid", false);
		map.put("reason", "Mohon maaf terjadi kesalahan internal pada service kami II.");
		return map;

	}
}
