package com.bank.mega.controller.registration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.ErrorHttpHandler;
import com.bank.mega.bean.ProfileInvestor;
import com.bank.mega.bean.RekeningDana;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@Controller
public class ProfileEsbnCtl extends BaseService {
	@RequestMapping("/esbn-profile")
	public String index(Model model, Authentication auth, HttpServletRequest request, HttpServletResponse response)
			throws ParseException {
		model.addAttribute("esbnProfiel", getInvestorBySid(auth));
		informationHeader(model, auth);
		return checkSessionValidity("registration/esbn-profile", auth, request, response);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> getInvestorBySid(Authentication auth) throws ParseException {
		Map<String, Object> map = new HashMap<String, Object>();
		ProfileInvestor investor = new ProfileInvestor();

		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, String> headerMap = new HashMap<>();

		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		// headerMap.put("secret-url", "/v1/investor/" + "sid48");
		headerMap.put("secret-url", "/v1/investor/" + auth.getName().toUpperCase());
		headerMap.put("secret-method", "GET");
		HttpRestResponse result = wsBody(wmsParentalWrapper + "esbn-connector/call-url/no-body", null, HttpMethod.GET,
				headerMap);

		HttpRestResponse wsResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName(), "GET", null, null,
				tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		String bodyResult = result.getBody();

		try {
			ErrorHttpHandler handler = mapperJsonToSingleDto(bodyResult, ErrorHttpHandler.class);
			if (handler.getCode() != 0) {
				Map<String, Object> errorMap = new HashMap<>();
				if (handler.getCode() == 102) {
					errorMap.put("valid", false);
					errorMap.put("reason",
							"SID investor tidak ditemukan, Silakan melakukan Registrasi ESBN terlebih dahulu.");
					return errorMap;
				} else {
					errorMap.put("valid", false);
					errorMap.put("reason", handler.getMessage());
					return errorMap;
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (bodyResult != null) {
			TblUserEsbn tblUserEsbn = getTblUser(auth);
			Map<String, Object> mapBody = mapperJsonToHashMap(bodyResult);
			Map<String, Object> mapp = mapperJsonToHashMap(wsResponse.getBody());
			Map<String, Object> mappp = mapperJsonToHashMap(new Gson().toJson(mapp.get("Result")));

			String nohp = null;
			String email = null;
			
			if (tblUserEsbn.getIsFirstEsbnRegist() == true) {
				nohp = (String) mappp.get("NoHandphone");
				email = (String) mappp.get("Email");
			} else {
				nohp = tblUserEsbn.getUserPhone();
				email = tblUserEsbn.getUserEmail();
			}

			investor.setAlamat((String) mapBody.get("Alamat"));
			investor.setSid((String) mapBody.get("Sid"));
			investor.setNama((String) mapBody.get("Nama"));
			investor.setNoIdentitas((String) mapBody.get("NoIdentitas"));
			investor.setTempatLahir((String) mapBody.get("TempatLahir"));

			// formated date tgl lahir
			String tglLahir = (String) mapBody.get("TglLahir");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date d = sdf.parse(tglLahir);
			String formattedDate = output.format(d);

			investor.setTglLahir(formattedDate);
			investor.setJenisKelamin((String) mapBody.get("JenisKelamin"));
			investor.setPekerjaan((String) mapBody.get("Pekerjaan"));
			investor.setProvinsi((String) mapBody.get("Provinsi"));
			investor.setKota((String) mapBody.get("Kota"));

			if (nohp != null) {
				investor.setNoHp(nohp);
			} else {
				nohp = "";
				investor.setNoHp(nohp);
			}

			investor.setEmail(email);

			String noRekeningDana = null;
			String noRekeningSurat = null;
			String namaSubregistry = null;
			String namaBank = null;
			String notlp = null;

			notlp = (String) mapBody.get("NoTelp");

			if (notlp != null) {
				investor.setNoTelp(notlp);
			} else {
				notlp = "";
				investor.setNoTelp(notlp);
			}

			List<Map<String, String>> listRekening = (List<Map<String, String>>) mapBody.get("RekeningDana");

			List<RekeningDana> listRekDana = new ArrayList<RekeningDana>();
			for (Map mapRek : listRekening) {
				RekeningDana dana = new RekeningDana();
				noRekeningDana = (String) mapRek.get("NoRek");
				namaBank = (String) mapRek.get("NamaBank");
				dana.setNoRekening(noRekeningDana);
				dana.setBank(namaBank);
				listRekDana.add(dana);
			}
			List<Map<String, String>> listRekeningSurat = (List<Map<String, String>>) mapBody.get("RekeningSB");

			if (listRekeningSurat.isEmpty() || "[]".equals(listRekeningSurat) || null == listRekeningSurat) {
				noRekeningSurat = "";
				namaSubregistry = "";

			} else {
				noRekeningSurat = listRekeningSurat.get(0).get("NoRek");
				namaSubregistry = listRekeningSurat.get(0).get("NamaSubregistry");
			}
			investor.setNamaSubregistry(namaSubregistry);
			investor.setRekeningSB(noRekeningSurat);
			investor.setRekeningDana(listRekDana);
			map.put("investor", investor);
			map.put("valid", true);

		} else {
			map.put("valid", false);
			map.put("reason", "Terjadi kesalahan di sistem kami. Segera hubungi Cabang Bank Mega terdekat.");
		}

		return map;
	}
}
