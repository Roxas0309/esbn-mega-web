package com.bank.mega.controller.dashboard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.model.NasabahDashboard;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.bank.mega.service.registration.RegistrationSvc;
import com.google.gson.Gson;

@Controller
public class DashboardController extends BaseService {

	@Autowired
	private RegistrationSvc registrationSvc;

	@RequestMapping("/")
	public String index(Model model, Authentication auth, HttpServletRequest request, HttpServletResponse response) {
		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, String> headerMap = new HashMap<>();
		Map<String, String> headerKTP = new HashMap<>();
		Map<String, Object> mapingBody = new HashMap<>();
		Map<String, Object> mapingResult = new HashMap<>();
		Map<String, Object> mapingktp = new HashMap<>();
		TblUserEsbn tblUserEsbn = getTblUser(auth);

		String nohp = null;
		String email = null;

		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBodyWms("/Inquiry.svc/GetCustomer/" + auth.getName(), "GET", null,
				headerMap);

		headerKTP.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		headerKTP.put("secret-field", "EKTP");
		headerKTP.put("secret-sid", auth.getName());
		HttpRestResponse resultKtp = wsBody(wmsParentalWrapper + "/registeration/GetOneDetailUser", null,
				HttpMethod.GET, headerKTP);

		mapingktp = mapperJsonToHashMap(resultKtp.getBody());

		mapingBody = mapperJsonToHashMap(httpRestResponse.getBody());
		mapingResult = mapperJsonToHashMap(new Gson().toJson(mapingBody.get("Result")));

		String noRekDana = null;
		String noRekSurat = null;
		@SuppressWarnings("unchecked")
		List<Map<String, String>> listRekening = (List<Map<String, String>>) mapingResult.get("RekeningDana");
		for (@SuppressWarnings("rawtypes")
		Map map : listRekening) {
			noRekDana = (String) map.get("NoRekening");
		}

		@SuppressWarnings("unchecked")
		List<Map<String, String>> listSurat = (List<Map<String, String>>) mapingResult.get("RekeningSuratBerharga");
		for (@SuppressWarnings("rawtypes")
		Map map : listSurat) {
			noRekSurat = (String) map.get("NoRekening");
		}

		if (tblUserEsbn.getIsFirstEsbnRegist() == true) {
			System.err.println("keluarin mapping " + new Gson().toJson(mapingResult));
			nohp = (String) mapingResult.get("NoHandphone");
			email = (String) mapingResult.get("Email");
		} else {
			nohp = tblUserEsbn.getUserPhone();
			email = tblUserEsbn.getUserEmail();
		}

		NasabahDashboard nasabah = new NasabahDashboard();
		nasabah.setAlamat((String) mapingResult.get("Alamat"));
		nasabah.setEmail(email);
		nasabah.setNama((String) mapingResult.get("Nama"));
		nasabah.setNoKtp((String) mapingktp.get("itemDescription"));
		nasabah.setNoHandphone(nohp);
		nasabah.setRekeningDana(noRekDana);
		nasabah.setSid((String) mapingResult.get("SID"));
		nasabah.setRekeningSuratBerharga(noRekSurat);

		model.addAttribute("nasabah", nasabah);
		informationHeader(model, auth);

		return checkSessionValidity("dashboard/index", auth, request, response);
	}

}
