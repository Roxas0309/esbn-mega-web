package com.bank.mega.model;

public class TblUserEsbn {

	private String userSid;
	
	private String userName;
	
	private String password;
	
	private Integer loginFailed;
	
	private String otpVerification;
	
//	@Column(name="is_first_login", nullable=false)
//	private Boolean isFirstLogin;
	
	private Boolean isActive;
	private Boolean isFirstEsbnRegist;
	private String userPhone;
	private String userEmail;
	

	
	
	
	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Boolean getIsFirstEsbnRegist() {
		return isFirstEsbnRegist;
	}

	public void setIsFirstEsbnRegist(Boolean isFirstEsbnRegist) {
		this.isFirstEsbnRegist = isFirstEsbnRegist;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserSid() {
		return userSid;
	}

	public void setUserSid(String userSid) {
		this.userSid = userSid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getLoginFailed() {
		return loginFailed;
	}

	public void setLoginFailed(Integer loginFailed) {
		this.loginFailed = loginFailed;
	}

//	public Boolean getIsFirstLogin() {
//		return isFirstLogin;
//	}
//
//	public void setIsFirstLogin(Boolean isFirstLogin) {
//		this.isFirstLogin = isFirstLogin;
//	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOtpVerification() {
		return otpVerification;
	}

	public void setOtpVerification(String otpVerification) {
		this.otpVerification = otpVerification;
	}
	
	
	
}
