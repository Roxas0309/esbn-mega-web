package com.bank.mega.model;


public class TblUserEsbnDtl {

	private long itemId;
	
	private String itemReference;
	
	private String itemDescription;
	
	private TblUserEsbn detailOwner;

	

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public String getItemReference() {
		return itemReference;
	}

	public void setItemReference(String itemReference) {
		this.itemReference = itemReference;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public TblUserEsbn getDetailOwner() {
		return detailOwner;
	}

	public void setDetailOwner(TblUserEsbn detailOwner) {
		this.detailOwner = detailOwner;
	}
	
	
}
