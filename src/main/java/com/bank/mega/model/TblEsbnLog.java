package com.bank.mega.model;

import java.util.Date;

public class TblEsbnLog {

	private Long logId;
	
	private String logSkala;
	
	private String logResource;
	
	private String logDescription;
	private String logOwner;
	
	private Date createdDate;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getLogSkala() {
		return logSkala;
	}

	public void setLogSkala(String logSkala) {
		this.logSkala = logSkala;
	}

	public String getLogResource() {
		return logResource;
	}

	public void setLogResource(String logResource) {
		this.logResource = logResource;
	}

	public String getLogDescription() {
		return logDescription;
	}

	public void setLogDescription(String logDescription) {
		this.logDescription = logDescription;
	}

	public String getLogOwner() {
		return logOwner;
	}

	public void setLogOwner(String logOwner) {
		this.logOwner = logOwner;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
}
