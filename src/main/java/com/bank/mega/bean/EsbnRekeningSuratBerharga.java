package com.bank.mega.bean;

public class EsbnRekeningSuratBerharga {
	private Long Id;
	private String NamaSubregistry;
	private String IdPartisipan;
	private String NamaPartisipan;
	private String CreatedAt;
	private String CreatedBy;
	private String ModifiedAt;
	private String ModifiedBy;
	private Long IdSubregistry;
	private String Sid;
	private String NoRek;
	private String Nama;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNamaSubregistry() {
		return NamaSubregistry;
	}

	public void setNamaSubregistry(String namaSubregistry) {
		NamaSubregistry = namaSubregistry;
	}

	public String getIdPartisipan() {
		return IdPartisipan;
	}

	public void setIdPartisipan(String idPartisipan) {
		IdPartisipan = idPartisipan;
	}

	public String getNamaPartisipan() {
		return NamaPartisipan;
	}

	public void setNamaPartisipan(String namaPartisipan) {
		NamaPartisipan = namaPartisipan;
	}

	public String getCreatedAt() {
		return CreatedAt;
	}

	public void setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getModifiedAt() {
		return ModifiedAt;
	}

	public void setModifiedAt(String modifiedAt) {
		ModifiedAt = modifiedAt;
	}

	public String getModifiedBy() {
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public Long getIdSubregistry() {
		return IdSubregistry;
	}

	public void setIdSubregistry(Long idSubregistry) {
		IdSubregistry = idSubregistry;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNoRek() {
		return NoRek;
	}

	public void setNoRek(String noRek) {
		NoRek = noRek;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

}
