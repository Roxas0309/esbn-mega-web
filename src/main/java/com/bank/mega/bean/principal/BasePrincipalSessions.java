package com.bank.mega.bean.principal;

public class BasePrincipalSessions {
     private String lastRequest;
     private CustomUserServiceBean principal;
     private String sessionId;
     private boolean expired;
     
     
	public String getLastRequest() {
		return lastRequest;
	}
	public void setLastRequest(String lastRequest) {
		this.lastRequest = lastRequest;
	}
	public CustomUserServiceBean getPrincipal() {
		return principal;
	}
	public void setPrincipal(CustomUserServiceBean principal) {
		this.principal = principal;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
     
     
}
