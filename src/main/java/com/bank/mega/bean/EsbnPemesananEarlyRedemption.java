package com.bank.mega.bean;

import java.math.BigInteger;

public class EsbnPemesananEarlyRedemption {
    private String NamaInvestor;
    private String KodeRedeem;
    private String Seri;
    private String TglSetelmen;
    private BigInteger SisaKepemilikan;
    private String Status;
    private String CreatedBy;
    private String TglRedeem;
    private BigInteger KelipatanRedeem;
    private BigInteger MinimalRedeem;
    private BigInteger MaksimalRedeem;
    private BigInteger Redeemable;
    private String KodePemesanan;
    private String Sid;
    private BigInteger Nominal;
    
	public String getNamaInvestor() {
		return NamaInvestor;
	}
	public void setNamaInvestor(String namaInvestor) {
		NamaInvestor = namaInvestor;
	}
	public String getKodeRedeem() {
		return KodeRedeem;
	}
	public void setKodeRedeem(String kodeRedeem) {
		KodeRedeem = kodeRedeem;
	}
	public String getSeri() {
		return Seri;
	}
	public void setSeri(String seri) {
		Seri = seri;
	}
	public String getTglSetelmen() {
		return TglSetelmen;
	}
	public void setTglSetelmen(String tglSetelmen) {
		TglSetelmen = tglSetelmen;
	}
	public BigInteger getSisaKepemilikan() {
		return SisaKepemilikan;
	}
	public void setSisaKepemilikan(BigInteger sisaKepemilikan) {
		SisaKepemilikan = sisaKepemilikan;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
	public String getTglRedeem() {
		return TglRedeem;
	}
	public void setTglRedeem(String tglRedeem) {
		TglRedeem = tglRedeem;
	}
	public BigInteger getKelipatanRedeem() {
		return KelipatanRedeem;
	}
	public void setKelipatanRedeem(BigInteger kelipatanRedeem) {
		KelipatanRedeem = kelipatanRedeem;
	}
	public BigInteger getMinimalRedeem() {
		return MinimalRedeem;
	}
	public void setMinimalRedeem(BigInteger minimalRedeem) {
		MinimalRedeem = minimalRedeem;
	}
	public BigInteger getMaksimalRedeem() {
		return MaksimalRedeem;
	}
	public void setMaksimalRedeem(BigInteger maksimalRedeem) {
		MaksimalRedeem = maksimalRedeem;
	}
	public BigInteger getRedeemable() {
		return Redeemable;
	}
	public void setRedeemable(BigInteger redeemable) {
		Redeemable = redeemable;
	}
	public String getKodePemesanan() {
		return KodePemesanan;
	}
	public void setKodePemesanan(String kodePemesanan) {
		KodePemesanan = kodePemesanan;
	}
	public String getSid() {
		return Sid;
	}
	public void setSid(String sid) {
		Sid = sid;
	}
	public BigInteger getNominal() {
		return Nominal;
	}
	public void setNominal(BigInteger nominal) {
		Nominal = nominal;
	}
    
    
    
}
