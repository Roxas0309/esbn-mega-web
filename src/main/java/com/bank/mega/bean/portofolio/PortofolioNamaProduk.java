package com.bank.mega.bean.portofolio;

public class PortofolioNamaProduk {
	private Long kodeProduk;
	private String namaProduk;

	

	public Long getKodeProduk() {
		return kodeProduk;
	}

	public void setKodeProduk(Long kodeProduk) {
		this.kodeProduk = kodeProduk;
	}

	public String getNamaProduk() {
		return namaProduk;
	}

	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}

}
