package com.bank.mega.bean.portofolio;

import java.math.BigInteger;
import java.util.Comparator;

public class PortofolioDataTableBean implements Comparable<PortofolioDataTableBean> {
	private String tipePemesanan;
	private String kodePemesanan;
	private String kodeReedeem;
	private String namaProduk;
	private BigInteger nominal;
	private BigInteger sisaNominal;
	private BigInteger sisaYangDapatDiredeem;
	private String tanggalSettelment;
	private String tanggalTransaksi;
	private String comparableDateTransaksi;
	private String tanggalJatuhTempo;
	
	

	public String getTanggalJatuhTempo() {
		return tanggalJatuhTempo;
	}

	public void setTanggalJatuhTempo(String tanggalJatuhTempo) {
		this.tanggalJatuhTempo = tanggalJatuhTempo;
	}

	public String getComparableDateTransaksi() {
		return comparableDateTransaksi;
	}

	public void setComparableDateTransaksi(String comparableDateTransaksi) {
		this.comparableDateTransaksi = comparableDateTransaksi;
	}

	public String getKodeReedeem() {
		return kodeReedeem;
	}

	public void setKodeReedeem(String kodeReedeem) {
		this.kodeReedeem = kodeReedeem;
	}

	public String getTipePemesanan() {
		return tipePemesanan;
	}

	public void setTipePemesanan(String tipePemesanan) {
		this.tipePemesanan = tipePemesanan;
	}

	public String getKodePemesanan() {
		return kodePemesanan;
	}

	public void setKodePemesanan(String kodePemesanan) {
		this.kodePemesanan = kodePemesanan;
	}

	public String getNamaProduk() {
		return namaProduk;
	}

	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}

	public BigInteger getNominal() {
		return nominal;
	}

	public void setNominal(BigInteger nominal) {
		this.nominal = nominal;
	}

	public BigInteger getSisaNominal() {
		return sisaNominal;
	}

	public void setSisaNominal(BigInteger sisaNominal) {
		this.sisaNominal = sisaNominal;
	}

	public String getTanggalTransaksi() {
		return tanggalTransaksi;
	}

	public void setTanggalTransaksi(String tanggalTransaksi) {
		this.tanggalTransaksi = tanggalTransaksi;
	}

	@Override
	public int compareTo(PortofolioDataTableBean o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public BigInteger getSisaYangDapatDiredeem() {
		return sisaYangDapatDiredeem;
	}

	public void setSisaYangDapatDiredeem(BigInteger sisaYangDapatDiredeem) {
		this.sisaYangDapatDiredeem = sisaYangDapatDiredeem;
	}

	public String getTanggalSettelment() {
		return tanggalSettelment;
	}

	public void setTanggalSettelment(String tanggalSettelment) {
		this.tanggalSettelment = tanggalSettelment;
	}

	public static Comparator<PortofolioDataTableBean> DATE_TRANSAKSI = new Comparator<PortofolioDataTableBean>() {
		public int compare(PortofolioDataTableBean comp1, PortofolioDataTableBean comp2) {
			String fruitName1 = comp1.comparableDateTransaksi;
			String fruitName2 = comp2.comparableDateTransaksi;
			return fruitName2.compareTo(fruitName1);
		}
	};

}
