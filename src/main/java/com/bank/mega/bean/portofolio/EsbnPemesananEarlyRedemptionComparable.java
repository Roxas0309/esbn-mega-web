package com.bank.mega.bean.portofolio;

import java.math.BigInteger;
import java.util.Comparator;

public class EsbnPemesananEarlyRedemptionComparable implements Comparable<EsbnPemesananEarlyRedemptionComparable>{
	  private String NamaInvestor;
	    private String KodeRedeem;
	    private String Seri;
	    private String TglSetelmen;
	    private BigInteger SisaKepemilikan;
	    private String Status;
	    private String CreatedBy;
	    private String TglRedeem;
	    private BigInteger KelipatanRedeem;
	    private BigInteger MinimalRedeem;
	    private BigInteger MaksimalRedeem;
	    private BigInteger Redeemable;
	    private String KodePemesanan;
	    private String Sid;
	    private BigInteger Nominal;
	    private String comparableDateTransaksi;
	    
	    
	    
	public String getNamaInvestor() {
			return NamaInvestor;
		}



		public void setNamaInvestor(String namaInvestor) {
			NamaInvestor = namaInvestor;
		}



		public String getKodeRedeem() {
			return KodeRedeem;
		}



		public void setKodeRedeem(String kodeRedeem) {
			KodeRedeem = kodeRedeem;
		}



		public String getSeri() {
			return Seri;
		}



		public void setSeri(String seri) {
			Seri = seri;
		}



		public String getTglSetelmen() {
			return TglSetelmen;
		}



		public void setTglSetelmen(String tglSetelmen) {
			TglSetelmen = tglSetelmen;
		}



		public BigInteger getSisaKepemilikan() {
			return SisaKepemilikan;
		}



		public void setSisaKepemilikan(BigInteger sisaKepemilikan) {
			SisaKepemilikan = sisaKepemilikan;
		}



		public String getStatus() {
			return Status;
		}



		public void setStatus(String status) {
			Status = status;
		}



		public String getCreatedBy() {
			return CreatedBy;
		}



		public void setCreatedBy(String createdBy) {
			CreatedBy = createdBy;
		}



		public String getTglRedeem() {
			return TglRedeem;
		}



		public void setTglRedeem(String tglRedeem) {
			TglRedeem = tglRedeem;
		}



		public BigInteger getKelipatanRedeem() {
			return KelipatanRedeem;
		}



		public void setKelipatanRedeem(BigInteger kelipatanRedeem) {
			KelipatanRedeem = kelipatanRedeem;
		}



		public BigInteger getMinimalRedeem() {
			return MinimalRedeem;
		}



		public void setMinimalRedeem(BigInteger minimalRedeem) {
			MinimalRedeem = minimalRedeem;
		}



		public BigInteger getMaksimalRedeem() {
			return MaksimalRedeem;
		}



		public void setMaksimalRedeem(BigInteger maksimalRedeem) {
			MaksimalRedeem = maksimalRedeem;
		}



		public BigInteger getRedeemable() {
			return Redeemable;
		}



		public void setRedeemable(BigInteger redeemable) {
			Redeemable = redeemable;
		}



		public String getKodePemesanan() {
			return KodePemesanan;
		}



		public void setKodePemesanan(String kodePemesanan) {
			KodePemesanan = kodePemesanan;
		}



		public String getSid() {
			return Sid;
		}



		public void setSid(String sid) {
			Sid = sid;
		}



		public BigInteger getNominal() {
			return Nominal;
		}



		public void setNominal(BigInteger nominal) {
			Nominal = nominal;
		}



		public String getComparableDateTransaksi() {
			return comparableDateTransaksi;
		}



		public void setComparableDateTransaksi(String comparableDateTransaksi) {
			this.comparableDateTransaksi = comparableDateTransaksi;
		}



	@Override
	public int compareTo(EsbnPemesananEarlyRedemptionComparable o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static Comparator<EsbnPemesananEarlyRedemptionComparable> DATE_TRANSAKSI = 
			new Comparator<EsbnPemesananEarlyRedemptionComparable>() {
		public int compare(EsbnPemesananEarlyRedemptionComparable comp1, EsbnPemesananEarlyRedemptionComparable comp2) {
			String fruitName1 = comp1.comparableDateTransaksi;
			String fruitName2 = comp2.comparableDateTransaksi;
			return fruitName1.compareTo(fruitName2);
		}
	};

}
