package com.bank.mega.bean;

import java.util.List;

public class RekeningSuratBerhargaInvestor {
	private String IdSubregistry;
	private String IdPartisipan;
	private String Sid;
	private String NoRek;
	private String Nama;
	private String myOtp;
	private List<RekeningSBTerpilihDummy> noRekTerpilihSB;

	

	public List<RekeningSBTerpilihDummy> getNoRekTerpilihSB() {
		return noRekTerpilihSB;
	}

	public void setNoRekTerpilihSB(List<RekeningSBTerpilihDummy> noRekTerpilihSB) {
		this.noRekTerpilihSB = noRekTerpilihSB;
	}

	public String getMyOtp() {
		return myOtp;
	}

	public void setMyOtp(String myOtp) {
		this.myOtp = myOtp;
	}
	public String getIdSubregistry() {
		return IdSubregistry;
	}

	public void setIdSubregistry(String idSubregistry) {
		IdSubregistry = idSubregistry;
	}

	public String getIdPartisipan() {
		return IdPartisipan;
	}

	public void setIdPartisipan(String idPartisipan) {
		IdPartisipan = idPartisipan;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNoRek() {
		return NoRek;
	}

	public void setNoRek(String noRek) {
		NoRek = noRek;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

}
