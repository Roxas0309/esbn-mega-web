package com.bank.mega.bean;

import java.math.BigDecimal;
import java.math.BigInteger;

public class SaveMyTransactionEsbn {
	private String Sid;
	private BigInteger IdSeri;
	private BigInteger IdRekDana;
	private BigInteger IdRekSB;
	private BigDecimal Nominal;
	public String getSid() {
		return Sid;
	}
	public void setSid(String sid) {
		Sid = sid;
	}
	public BigInteger getIdSeri() {
		return IdSeri;
	}
	public void setIdSeri(BigInteger idSeri) {
		IdSeri = idSeri;
	}
	public BigInteger getIdRekDana() {
		return IdRekDana;
	}
	public void setIdRekDana(BigInteger idRekDana) {
		IdRekDana = idRekDana;
	}
	public BigInteger getIdRekSB() {
		return IdRekSB;
	}
	public void setIdRekSB(BigInteger idRekSB) {
		IdRekSB = idRekSB;
	}
	public BigDecimal getNominal() {
		return Nominal;
	}
	public void setNominal(BigDecimal nominal) {
		Nominal = nominal;
	}
	
	
}
