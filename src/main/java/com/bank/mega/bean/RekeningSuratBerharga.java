package com.bank.mega.bean;

public class RekeningSuratBerharga {
	private String IdPartisipan;
	private String IdSubregistry;
	private String Nama;
	private String NamaPartisipan;
	private String NamaSubRegistry;
	private String NoRekening;

	public String getIdPartisipan() {
		return IdPartisipan;
	}

	public void setIdPartisipan(String idPartisipan) {
		IdPartisipan = idPartisipan;
	}

	public String getIdSubregistry() {
		return IdSubregistry;
	}

	public void setIdSubregistry(String idSubregistry) {
		IdSubregistry = idSubregistry;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

	public String getNamaPartisipan() {
		return NamaPartisipan;
	}

	public void setNamaPartisipan(String namaPartisipan) {
		NamaPartisipan = namaPartisipan;
	}

	public String getNamaSubRegistry() {
		return NamaSubRegistry;
	}

	public void setNamaSubRegistry(String namaSubRegistry) {
		NamaSubRegistry = namaSubRegistry;
	}

	public String getNoRekening() {
		return NoRekening;
	}

	public void setNoRekening(String noRekening) {
		NoRekening = noRekening;
	}

}
