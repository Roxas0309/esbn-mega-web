package com.bank.mega.bean;

public class RekeningSBTerpilihDummy {
	private RekeningSuratBerharga noRekTerpilih;

	public RekeningSuratBerharga  getNoRekTerpilih() {
		return noRekTerpilih;
	}

	public void setNoRekTerpilih(RekeningSuratBerharga noRekTerpilih) {
		this.noRekTerpilih = noRekTerpilih;
	}
}
