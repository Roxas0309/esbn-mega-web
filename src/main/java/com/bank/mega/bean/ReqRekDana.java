package com.bank.mega.bean;

public class ReqRekDana {
	private String IdBank;
	private String Sid;
	private String NoRek;
	private String Nama;

	public String getIdBank() {
		return IdBank;
	}

	public void setIdBank(String idBank) {
		IdBank = idBank;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public String getNoRek() {
		return NoRek;
	}

	public void setNoRek(String noRek) {
		NoRek = noRek;
	}

	public String getNama() {
		return Nama;
	}

	public void setNama(String nama) {
		Nama = nama;
	}

}
