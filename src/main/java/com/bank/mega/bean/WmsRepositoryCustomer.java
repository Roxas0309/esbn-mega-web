package com.bank.mega.bean;

import java.util.List;

public class WmsRepositoryCustomer {
      private String Alamat;
      private String Email;
      private String JenisKelamin;
      private String KodeJenisKelamin;
      private String KodeKota;
      private String KodePekerjaan;
      private String KodePropinsi;
      private String Kota;
      private String Nama;
      private String NoHandphone;
      private String NoKTP;
      private String NoTelepon;
      private String Pekerjaan;
      private String Propinsi;
      private WmsRepositoryRekeningDana RekeningDana;
      private List<WmsRepositoryRekeningSuratBerharga> RekeningSuratBerharga;
      private WmsRepositoryRiskProfile RiskProfile;
      private String SID;
      private String TanggalLahir;
      private String TempatLahir;
	public String getAlamat() {
		return Alamat;
	}
	public void setAlamat(String alamat) {
		Alamat = alamat;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getJenisKelamin() {
		return JenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		JenisKelamin = jenisKelamin;
	}
	public String getKodeJenisKelamin() {
		return KodeJenisKelamin;
	}
	public void setKodeJenisKelamin(String kodeJenisKelamin) {
		KodeJenisKelamin = kodeJenisKelamin;
	}
	public String getKodeKota() {
		return KodeKota;
	}
	public void setKodeKota(String kodeKota) {
		KodeKota = kodeKota;
	}
	public String getKodePekerjaan() {
		return KodePekerjaan;
	}
	public void setKodePekerjaan(String kodePekerjaan) {
		KodePekerjaan = kodePekerjaan;
	}
	public String getKodePropinsi() {
		return KodePropinsi;
	}
	public void setKodePropinsi(String kodePropinsi) {
		KodePropinsi = kodePropinsi;
	}
	public String getKota() {
		return Kota;
	}
	public void setKota(String kota) {
		Kota = kota;
	}
	public String getNama() {
		return Nama;
	}
	public void setNama(String nama) {
		Nama = nama;
	}
	public String getNoHandphone() {
		return NoHandphone;
	}
	public void setNoHandphone(String noHandphone) {
		NoHandphone = noHandphone;
	}
	public String getNoKTP() {
		return NoKTP;
	}
	public void setNoKTP(String noKTP) {
		NoKTP = noKTP;
	}
	public String getNoTelepon() {
		return NoTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		NoTelepon = noTelepon;
	}
	public String getPekerjaan() {
		return Pekerjaan;
	}
	public void setPekerjaan(String pekerjaan) {
		Pekerjaan = pekerjaan;
	}
	public String getPropinsi() {
		return Propinsi;
	}
	public void setPropinsi(String propinsi) {
		Propinsi = propinsi;
	}
	public WmsRepositoryRekeningDana getRekeningDana() {
		return RekeningDana;
	}
	public void setRekeningDana(WmsRepositoryRekeningDana rekeningDana) {
		RekeningDana = rekeningDana;
	}
	public List<WmsRepositoryRekeningSuratBerharga> getRekeningSuratBerharga() {
		return RekeningSuratBerharga;
	}
	public void setRekeningSuratBerharga(List<WmsRepositoryRekeningSuratBerharga> rekeningSuratBerharga) {
		RekeningSuratBerharga = rekeningSuratBerharga;
	}
	public WmsRepositoryRiskProfile getRiskProfile() {
		return RiskProfile;
	}
	public void setRiskProfile(WmsRepositoryRiskProfile riskProfile) {
		RiskProfile = riskProfile;
	}
	public String getSID() {
		return SID;
	}
	public void setSID(String sID) {
		SID = sID;
	}
	public String getTanggalLahir() {
		return TanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		TanggalLahir = tanggalLahir;
	}
	public String getTempatLahir() {
		return TempatLahir;
	}
	public void setTempatLahir(String tempatLahir) {
		TempatLahir = tempatLahir;
	}
      
      
      
      
}
