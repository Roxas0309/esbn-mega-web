package com.bank.mega.bean;

import java.math.BigInteger;

public class SaveMyTransactionEarlyRedemption {
	private String KodePemesanan;
	private String Sid;
	private BigInteger Nominal;
	private String rekeningDana;
	private String myOtp;
	
	

	public String getRekeningDana() {
		return rekeningDana;
	}

	public void setRekeningDana(String rekeningDana) {
		this.rekeningDana = rekeningDana;
	}

	public String getMyOtp() {
		return myOtp;
	}

	public void setMyOtp(String myOtp) {
		this.myOtp = myOtp;
	}

	public String getKodePemesanan() {
		return KodePemesanan;
	}

	public void setKodePemesanan(String kodePemesanan) {
		KodePemesanan = kodePemesanan;
	}

	public String getSid() {
		return Sid;
	}

	public void setSid(String sid) {
		Sid = sid;
	}

	public BigInteger getNominal() {
		return Nominal;
	}

	public void setNominal(BigInteger nominal) {
		Nominal = nominal;
	}

}
