package com.bank.mega.bean;

import java.math.BigDecimal;

public class EsbnAllSeriProductPemesanan {
    private Long Id;
    private String Seri;
    private BigDecimal TingkatKupon;
    private BigDecimal BatasBawahKupon;
    private BigDecimal TingkatKuponAwal;
    private BigDecimal Spread;
    private String JenisKupon;
    private String TglBayarKupon;
    private String TglSetelmen;
    private String TglJatuhTempo;
    private String TglMulaiPemesanan;
    private String TglAkhirPemesanan;
    private BigDecimal MinPemesanan;
    private BigDecimal MaxPemesanan;
    private BigDecimal KelipatanPemesanan;
    private BigDecimal Target;
    private BigDecimal FrekuensiKupon;
    private String Tradability;
    private String LinkMemo;
	public Long getId() {
		return Id;
	}
	public void setId(Long Id) {
		this.Id = Id;
	}
	public String getSeri() {
		return Seri;
	}
	public void setSeri(String Seri) {
		this.Seri = Seri;
	}
	public BigDecimal getTingkatKupon() {
		return TingkatKupon;
	}
	public void setTingkatKupon(BigDecimal TingkatKupon) {
		this.TingkatKupon = TingkatKupon;
	}
	public BigDecimal getBatasBawahKupon() {
		return BatasBawahKupon;
	}
	public void setBatasBawahKupon(BigDecimal BatasBawahKupon) {
		this.BatasBawahKupon = BatasBawahKupon;
	}
	public BigDecimal getTingkatKuponAwal() {
		return TingkatKuponAwal;
	}
	public void setTingkatKuponAwal(BigDecimal TingkatKuponAwal) {
		this.TingkatKuponAwal = TingkatKuponAwal;
	}
	public BigDecimal getSpread() {
		return Spread;
	}
	public void setSpread(BigDecimal Spread) {
		this.Spread = Spread;
	}
	public String getJenisKupon() {
		return JenisKupon;
	}
	public void setJenisKupon(String JenisKupon) {
		this.JenisKupon = JenisKupon;
	}
	public String getTglBayarKupon() {
		return TglBayarKupon;
	}
	public void setTglBayarKupon(String TglBayarKupon) {
		this.TglBayarKupon = TglBayarKupon;
	}
	public String getTglSetelmen() {
		return TglSetelmen;
	}
	public void setTglSetelmen(String TglSetelmen) {
		this.TglSetelmen = TglSetelmen;
	}
	public String getTglJatuhTempo() {
		return TglJatuhTempo;
	}
	public void setTglJatuhTempo(String TglJatuhTempo) {
		this.TglJatuhTempo = TglJatuhTempo;
	}
	public String getTglMulaiPemesanan() {
		return TglMulaiPemesanan;
	}
	public void setTglMulaiPemesanan(String TglMulaiPemesanan) {
		this.TglMulaiPemesanan = TglMulaiPemesanan;
	}
	public String getTglAkhirPemesanan() {
		return TglAkhirPemesanan;
	}
	public void setTglAkhirPemesanan(String TglAkhirPemesanan) {
		this.TglAkhirPemesanan = TglAkhirPemesanan;
	}
	public BigDecimal getMinPemesanan() {
		return MinPemesanan;
	}
	public void setMinPemesanan(BigDecimal MinPemesanan) {
		this.MinPemesanan = MinPemesanan;
	}
	public BigDecimal getMaxPemesanan() {
		return MaxPemesanan;
	}
	public void setMaxPemesanan(BigDecimal MaxPemesanan) {
		this.MaxPemesanan = MaxPemesanan;
	}
	public BigDecimal getKelipatanPemesanan() {
		return KelipatanPemesanan;
	}
	public void setKelipatanPemesanan(BigDecimal KelipatanPemesanan) {
		this.KelipatanPemesanan = KelipatanPemesanan;
	}
	public BigDecimal getTarget() {
		return Target;
	}
	public void setTarget(BigDecimal Target) {
		this.Target = Target;
	}
	public BigDecimal getFrekuensiKupon() {
		return FrekuensiKupon;
	}
	public void setFrekuensiKupon(BigDecimal FrekuensiKupon) {
		this.FrekuensiKupon = FrekuensiKupon;
	}
	public String getTradability() {
		return Tradability;
	}
	public void setTradability(String Tradability) {
		this.Tradability = Tradability;
	}
	public String getLinkMemo() {
		return LinkMemo;
	}
	public void setLinkMemo(String LinkMemo) {
		this.LinkMemo = LinkMemo;
	}
    
    
}
