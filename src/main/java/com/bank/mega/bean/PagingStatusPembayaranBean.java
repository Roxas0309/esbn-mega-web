package com.bank.mega.bean;

import java.util.ArrayList;
import java.util.List;

public class PagingStatusPembayaranBean {
    private Long PageNumber;
    private Long PageSize;
    private Long TotalPage;
    private Long TotalRecord;
    private List<StatusPembayaranBean> Records = new ArrayList<>();
	public Long getPageNumber() {
		return PageNumber;
	}
	public void setPageNumber(Long pageNumber) {
		PageNumber = pageNumber;
	}
	public Long getPageSize() {
		return PageSize;
	}
	public void setPageSize(Long pageSize) {
		PageSize = pageSize;
	}
	public Long getTotalPage() {
		return TotalPage;
	}
	public void setTotalPage(Long totalPage) {
		TotalPage = totalPage;
	}
	public Long getTotalRecord() {
		return TotalRecord;
	}
	public void setTotalRecord(Long totalRecord) {
		TotalRecord = totalRecord;
	}
	public List<StatusPembayaranBean> getRecords() {
		return Records;
	}
	public void setRecords(List<StatusPembayaranBean> records) {
		Records = records;
	}
    
    
}
