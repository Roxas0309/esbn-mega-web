package com.bank.mega.bean;

import java.math.BigInteger;

public class EsbnMetamorphosisKodePemesan {
    private  EsbnPemesananTransaksi esbnPemesananTransaksi;
    private  BigInteger maximumPemesananRedeem;
    private  BigInteger redeemable;
    private  String transaksiNominal;
    
    
    
    
	public String getTransaksiNominal() {
		return transaksiNominal;
	}
	public void setTransaksiNominal(String transaksiNominal) {
		this.transaksiNominal = transaksiNominal;
	}
	public BigInteger getRedeemable() {
		return redeemable;
	}
	public void setRedeemable(BigInteger redeemable) {
		this.redeemable = redeemable;
	}
	public EsbnPemesananTransaksi getEsbnPemesananTransaksi() {
		return esbnPemesananTransaksi;
	}
	public void setEsbnPemesananTransaksi(EsbnPemesananTransaksi esbnPemesananTransaksi) {
		this.esbnPemesananTransaksi = esbnPemesananTransaksi;
	}
	public BigInteger getMaximumPemesananRedeem() {
		return maximumPemesananRedeem;
	}
	public void setMaximumPemesananRedeem(BigInteger maximumPemesananRedeem) {
		this.maximumPemesananRedeem = maximumPemesananRedeem;
	}
	
    
    
    
}
