package com.bank.mega.bean;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class EsbnPemesananTransaksi {
    private String NamaInvestor;
    private String Seri;
    private String KodePemesanan;
    private BigDecimal TingkatKupon;
    private String TglBayarKupon;
    private String TglSetelmen;
    private String TglJatuhTempo;
    private String KodeBilling;
    private BigInteger NominalKupon;
    private BigInteger NominalKuponPerUnit;
    private BigInteger NominalPerUnit;
    private BigInteger NominalKuponPertama;
    private BigInteger Redeem;
    private BigInteger SisaKepemilikan;
    private String IdStatus;
    private String Status;
    private String BatasWaktuBayar;
    private String NTPN;
    private String TglPemesanan;
    private String CreatedBy;
    private EsbnRekeningDana RekeningDana;
    private EsbnRekeningSuratBerharga  RekeningSB;
    private String Sid;
    private Long IdSeri;
    private BigInteger Nominal;
    private BigInteger IdRekDana;
    private BigInteger IdRekSb;
	public String getNamaInvestor() {
		return NamaInvestor;
	}
	public void setNamaInvestor(String namaInvestor) {
		NamaInvestor = namaInvestor;
	}
	public String getSeri() {
		return Seri;
	}
	public void setSeri(String seri) {
		Seri = seri;
	}
	public String getKodePemesanan() {
		return KodePemesanan;
	}
	public void setKodePemesanan(String kodePemesanan) {
		KodePemesanan = kodePemesanan;
	}
	
	
	public BigDecimal getTingkatKupon() {
		return TingkatKupon;
	}
	public void setTingkatKupon(BigDecimal tingkatKupon) {
		TingkatKupon = tingkatKupon;
	}
	public String getTglBayarKupon() {
		return TglBayarKupon;
	}
	public void setTglBayarKupon(String tglBayarKupon) {
		TglBayarKupon = tglBayarKupon;
	}
	public String getTglSetelmen() {
		return TglSetelmen;
	}
	public void setTglSetelmen(String tglSetelmen) {
		TglSetelmen = tglSetelmen;
	}
	public String getTglJatuhTempo() {
		return TglJatuhTempo;
	}
	public void setTglJatuhTempo(String tglJatuhTempo) {
		TglJatuhTempo = tglJatuhTempo;
	}
	public String getKodeBilling() {
		return KodeBilling;
	}
	public void setKodeBilling(String kodeBilling) {
		KodeBilling = kodeBilling;
	}
	public BigInteger getNominalKupon() {
		return NominalKupon;
	}
	public void setNominalKupon(BigInteger nominalKupon) {
		NominalKupon = nominalKupon;
	}
	public BigInteger getNominalKuponPerUnit() {
		return NominalKuponPerUnit;
	}
	public void setNominalKuponPerUnit(BigInteger nominalKuponPerUnit) {
		NominalKuponPerUnit = nominalKuponPerUnit;
	}
	public BigInteger getNominalPerUnit() {
		return NominalPerUnit;
	}
	public void setNominalPerUnit(BigInteger nominalPerUnit) {
		NominalPerUnit = nominalPerUnit;
	}
	public BigInteger getNominalKuponPertama() {
		return NominalKuponPertama;
	}
	public void setNominalKuponPertama(BigInteger nominalKuponPertama) {
		NominalKuponPertama = nominalKuponPertama;
	}
	public BigInteger getRedeem() {
		return Redeem;
	}
	public void setRedeem(BigInteger redeem) {
		Redeem = redeem;
	}
	public BigInteger getSisaKepemilikan() {
		return SisaKepemilikan;
	}
	public void setSisaKepemilikan(BigInteger sisaKepemilikan) {
		SisaKepemilikan = sisaKepemilikan;
	}
	public String getIdStatus() {
		return IdStatus;
	}
	public void setIdStatus(String idStatus) {
		IdStatus = idStatus;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getBatasWaktuBayar() {
		return BatasWaktuBayar;
	}
	public void setBatasWaktuBayar(String batasWaktuBayar) {
		BatasWaktuBayar = batasWaktuBayar;
	}
	public String getNTPN() {
		return NTPN;
	}
	public void setNTPN(String nTPN) {
		NTPN = nTPN;
	}
	public String getTglPemesanan() {
		return TglPemesanan;
	}
	public void setTglPemesanan(String tglPemesanan) {
		TglPemesanan = tglPemesanan;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
	public EsbnRekeningDana getRekeningDana() {
		return RekeningDana;
	}
	public void setRekeningDana(EsbnRekeningDana rekeningDana) {
		RekeningDana = rekeningDana;
	}
	public EsbnRekeningSuratBerharga getRekeningSB() {
		return RekeningSB;
	}
	public void setRekeningSB(EsbnRekeningSuratBerharga rekeningSB) {
		RekeningSB = rekeningSB;
	}
	public String getSid() {
		return Sid;
	}
	public void setSid(String sid) {
		Sid = sid;
	}
	public Long getIdSeri() {
		return IdSeri;
	}
	public void setIdSeri(Long idSeri) {
		IdSeri = idSeri;
	}
	public BigInteger getNominal() {
		return Nominal;
	}
	public void setNominal(BigInteger nominal) {
		Nominal = nominal;
	}
	public BigInteger getIdRekDana() {
		return IdRekDana;
	}
	public void setIdRekDana(BigInteger idRekDana) {
		IdRekDana = idRekDana;
	}
	public BigInteger getIdRekSb() {
		return IdRekSb;
	}
	public void setIdRekSb(BigInteger idRekSb) {
		IdRekSb = idRekSb;
	}
    
    
}
