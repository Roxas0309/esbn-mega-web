package com.bank.mega.bean;

import com.bank.mega.model.TblUserEsbn;

public class TblUserEsbnV2 {
	private TblUserEsbn esbn;
	private int errorCode;

	public TblUserEsbn getEsbn() {
		return esbn;
	}

	public void setEsbn(TblUserEsbn esbn) {
		this.esbn = esbn;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

}
