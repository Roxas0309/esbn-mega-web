package com.bank.mega.bean;

public class EsbnRekeningDana {
    private Long Id;
    private String NamaBank;
    private String CreatedAt;
    private String CreatedBy;
    private String ModifiedAt;
    private String ModifiedBy;
    private String IdBank;
    private String Sid;
    private String NoRek;
    private String Nama;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getNamaBank() {
		return NamaBank;
	}
	public void setNamaBank(String namaBank) {
		NamaBank = namaBank;
	}
	public String getCreatedAt() {
		return CreatedAt;
	}
	public void setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
	public String getModifiedAt() {
		return ModifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		ModifiedAt = modifiedAt;
	}
	public String getModifiedBy() {
		return ModifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}
	public String getIdBank() {
		return IdBank;
	}
	public void setIdBank(String idBank) {
		IdBank = idBank;
	}
	public String getSid() {
		return Sid;
	}
	public void setSid(String sid) {
		Sid = sid;
	}
	public String getNoRek() {
		return NoRek;
	}
	public void setNoRek(String noRek) {
		NoRek = noRek;
	}
	public String getNama() {
		return Nama;
	}
	public void setNama(String nama) {
		Nama = nama;
	}
    
    
}