package com.bank.mega.bean;

public class TblEsbnLogComponentWebService {
      private String url;
      private String method;
      private String header;
      private String body;
      private String resultShow;
      private String userId;
    
      
      
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getResultShow() {
		return resultShow;
	}
	public void setResultShow(String resultShow) {
		this.resultShow = resultShow;
	}
      
      
}
