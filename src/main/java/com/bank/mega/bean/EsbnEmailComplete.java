package com.bank.mega.bean;

import java.math.BigInteger;

public class EsbnEmailComplete {

	private String email;
	private String namaUser;
	private String userSid;
	private String namaSeri;
	private String kodePemesanan;
	private String nominalPemesanan;
	private String waktuPembayaran;
	private String kodeNtpn;
	
	
	
	public String getUserSid() {
		return userSid;
	}
	public void setUserSid(String userSid) {
		this.userSid = userSid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNamaUser() {
		return namaUser;
	}
	public void setNamaUser(String namaUser) {
		this.namaUser = namaUser;
	}
	public String getNamaSeri() {
		return namaSeri;
	}
	public void setNamaSeri(String namaSeri) {
		this.namaSeri = namaSeri;
	}
	public String getKodePemesanan() {
		return kodePemesanan;
	}
	public void setKodePemesanan(String kodePemesanan) {
		this.kodePemesanan = kodePemesanan;
	}
	
	public String getNominalPemesanan() {
		return nominalPemesanan;
	}
	public void setNominalPemesanan(String nominalPemesanan) {
		this.nominalPemesanan = nominalPemesanan;
	}
	public String getWaktuPembayaran() {
		return waktuPembayaran;
	}
	public void setWaktuPembayaran(String waktuPembayaran) {
		this.waktuPembayaran = waktuPembayaran;
	}
	public String getKodeNtpn() {
		return kodeNtpn;
	}
	public void setKodeNtpn(String kodeNtpn) {
		this.kodeNtpn = kodeNtpn;
	}
	
	
}
