package com.bank.mega.bean.adjustor;

import com.bank.mega.bean.WmsRepositoryCustomer;

public class InquirySvcGetCustomerAdj {
    private String ErrorCode;
    private Boolean IsSuccess;
    private String Message;
    private String ResponseCode;
    private String SolutionCode;
    private WmsRepositoryCustomer Result;
	public String getErrorCode() {
		return ErrorCode;
	}
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	public Boolean getIsSuccess() {
		return IsSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		IsSuccess = isSuccess;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	public String getSolutionCode() {
		return SolutionCode;
	}
	public void setSolutionCode(String solutionCode) {
		SolutionCode = solutionCode;
	}
	public WmsRepositoryCustomer getResult() {
		return Result;
	}
	public void setResult(WmsRepositoryCustomer result) {
		Result = result;
	}
	
    
    
}  
