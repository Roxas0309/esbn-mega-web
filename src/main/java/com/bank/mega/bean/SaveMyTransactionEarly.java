package com.bank.mega.bean;

import java.math.BigInteger;

public class SaveMyTransactionEarly {
	private String KodePemesanan;
	private String Sid;
	private BigInteger Nominal;
	public String getKodePemesanan() {
		return KodePemesanan;
	}
	public void setKodePemesanan(String kodePemesanan) {
		KodePemesanan = kodePemesanan;
	}
	public String getSid() {
		return Sid;
	}
	public void setSid(String sid) {
		Sid = sid;
	}
	public BigInteger getNominal() {
		return Nominal;
	}
	public void setNominal(BigInteger nominal) {
		Nominal = nominal;
	}
	
}
