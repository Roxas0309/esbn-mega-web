package com.bank.mega.bean;

import java.util.List;

public class EsbnRegistrationBean {
	private String sid;
	private String namaNasabah;
	private String noKtp;
	private String tempatLahir;
	private String tanggalLahir;
	private String jenisKelamin;
	private String jenisPekerjaan;
	private String alamat;
	private String provinsi;
	private String kota;
	private String noTelepon;
	private String noHandphone;
	private String email;
	private String namaSubregistry;
	private String noRekSuratBerharga;
	private List<RekeningDana> noRekeningDana;
	private String kodeJenisKelamin;
	private String kodePekerjaan;
	private String kodeKota;
	private String kodePropinsi;
	
	private String idSubregistry;
	private String idPartisipan;
	private String namaPartisipan;
	private String rekDanaString;

	public String getRekDanaString() {
		return rekDanaString;
	}

	public void setRekDanaString(String rekDanaString) {
		this.rekDanaString = rekDanaString;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public String getNamaNasabah() {
		return namaNasabah;
	}

	public void setNamaNasabah(String namaNasabah) {
		this.namaNasabah = namaNasabah;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getJenisPekerjaan() {
		return jenisPekerjaan;
	}

	public void setJenisPekerjaan(String jenisPekerjaan) {
		this.jenisPekerjaan = jenisPekerjaan;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public String getNoTelepon() {
		return noTelepon;
	}

	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}

	public String getNoHandphone() {
		return noHandphone;
	}

	public void setNoHandphone(String noHandphone) {
		this.noHandphone = noHandphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNamaSubregistry() {
		return namaSubregistry;
	}

	public void setNamaSubregistry(String namaSubregistry) {
		this.namaSubregistry = namaSubregistry;
	}

	public String getNoRekSuratBerharga() {
		return noRekSuratBerharga;
	}

	public void setNoRekSuratBerharga(String noRekSuratBerharga) {
		this.noRekSuratBerharga = noRekSuratBerharga;
	}

	public List<RekeningDana> getNoRekeningDana() {
		return noRekeningDana;
	}

	public void setNoRekeningDana(List<RekeningDana> noRekeningDana) {
		this.noRekeningDana = noRekeningDana;
	}

	public String getKodeJenisKelamin() {
		return kodeJenisKelamin;
	}

	public void setKodeJenisKelamin(String kodeJenisKelamin) {
		this.kodeJenisKelamin = kodeJenisKelamin;
	}

	public String getKodePekerjaan() {
		return kodePekerjaan;
	}

	public void setKodePekerjaan(String kodePekerjaan) {
		this.kodePekerjaan = kodePekerjaan;
	}

	public String getKodeKota() {
		return kodeKota;
	}

	public void setKodeKota(String kodeKota) {
		this.kodeKota = kodeKota;
	}

	public String getKodePropinsi() {
		return kodePropinsi;
	}

	public void setKodePropinsi(String kodePropinsi) {
		this.kodePropinsi = kodePropinsi;
	}

	public String getIdSubregistry() {
		return idSubregistry;
	}

	public void setIdSubregistry(String idSubregistry) {
		this.idSubregistry = idSubregistry;
	}

	public String getIdPartisipan() {
		return idPartisipan;
	}

	public void setIdPartisipan(String idPartisipan) {
		this.idPartisipan = idPartisipan;
	}

	public String getNamaPartisipan() {
		return namaPartisipan;
	}

	public void setNamaPartisipan(String namaPartisipan) {
		this.namaPartisipan = namaPartisipan;
	}

}
