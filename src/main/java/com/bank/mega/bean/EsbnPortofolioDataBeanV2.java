package com.bank.mega.bean;

import java.math.BigInteger;

public class EsbnPortofolioDataBeanV2 {
	private BigInteger nominalPemesananAwal;
	private BigInteger nominalYangSudahDiredeem;
	private BigInteger sisaKepemilikanTerakhir;
	private String tanggalJatuhTempo;
	private String namaProduk;
	public BigInteger getNominalPemesananAwal() {
		return nominalPemesananAwal;
	}
	public void setNominalPemesananAwal(BigInteger nominalPemesananAwal) {
		this.nominalPemesananAwal = nominalPemesananAwal;
	}
	public BigInteger getNominalYangSudahDiredeem() {
		return nominalYangSudahDiredeem;
	}
	public void setNominalYangSudahDiredeem(BigInteger nominalYangSudahDiredeem) {
		this.nominalYangSudahDiredeem = nominalYangSudahDiredeem;
	}
	public BigInteger getSisaKepemilikanTerakhir() {
		return sisaKepemilikanTerakhir;
	}
	public void setSisaKepemilikanTerakhir(BigInteger sisaKepemilikanTerakhir) {
		this.sisaKepemilikanTerakhir = sisaKepemilikanTerakhir;
	}
	public String getTanggalJatuhTempo() {
		return tanggalJatuhTempo;
	}
	public void setTanggalJatuhTempo(String tanggalJatuhTempo) {
		this.tanggalJatuhTempo = tanggalJatuhTempo;
	}
	public String getNamaProduk() {
		return namaProduk;
	}
	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}
	
	
}
