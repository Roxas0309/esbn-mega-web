package com.bank.mega;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.bank.mega.bean.TblUserEsbnV2;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.CustomUserService;
import com.bank.mega.service.registration.RegistrationSvc;
import com.google.gson.Gson;

@SpringBootApplication
public class SistemApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SistemApplication.class, args);
	}

	public static boolean isLimitedExceededSession(String username, SessionRegistry sessionRegistry) {

		for (Object principal : sessionRegistry.getAllPrincipals()) {
			System.err.println("ses  " + new Gson().toJson(principal));
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				if (service.getUsername().equalsIgnoreCase(username)) {
					System.err.println("user " + username + " telah memiliki session");
					return false;
				}
			}
		}
		return false;
	}

	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, SessionRegistry sessionRegistry,
			HttpServletRequest request, HttpSession session,RegistrationSvc regist) throws Exception {
		builder.userDetailsService(new UserDetailsService() {
//HttpSession session
			@Override
			public UserDetails loadUserByUsername(String userValidation) throws UsernameNotFoundException {
				String password = request.getParameter("password");
				
				if (isLimitedExceededSession(userValidation, sessionRegistry)) {
					session.setAttribute("reason-error", "Mohon maaf, saat ini user Anda sedang Login di perangkat yang lain");
					return null;
				} else {
					int errorCode = 0;
					TblUserEsbnV2 tblUserEsbnV2 = regist.getDetail(userValidation, password,errorCode);
					System.err.println("error : " + errorCode);
					if(tblUserEsbnV2.getEsbn()!=null) {
						regist.noteUserLogin(tblUserEsbnV2.getEsbn());
					}
					else {
						if(tblUserEsbnV2.getErrorCode()==402) {
						session.setAttribute("reason-error", "Mohon maaf, User Id yang Anda Gunakan Telah Terblokir. Mohon Datang Ke Cabang Bank Mega Terdekat Agar Anda Dapat Menggunakan User Id Anda Lagi.");
						}
						else if(tblUserEsbnV2.getErrorCode()==403) {
							session.setAttribute("reason-error", "Mohon maaf, User Id yang Anda Gunakan Telah di Non-Aktifkan. Mohon Datang Ke Cabang Bank Mega Terdekat Agar Anda Dapat Menggunakan User Id Anda Lagi.");
						}
					}
					return new CustomUserService(tblUserEsbnV2.getEsbn());
				}
			}
		});
	}
}
