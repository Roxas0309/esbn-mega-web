package com.bank.mega.service.registration;

import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.bank.mega.bean.TblEsbnLoginLogBean;
import com.bank.mega.bean.TblUserEsbnV2;
import com.bank.mega.bean.UserBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.model.TblUserEsbn;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@Service
public class RegistrationSvc extends BaseService{

	public void noteUserLogin(TblUserEsbn tblUserEsbn) {
		TblEsbnLoginLogBean bean = new TblEsbnLoginLogBean();
		bean.setLogUserId(tblUserEsbn.getUserSid());
		bean.setLogUserName(tblUserEsbn.getUserName());
		bean.setLogUserTypeCode("nasabah");
		bean.setLogUserTypeCodeName("Nasabah");
		HttpRestResponse response = wsBody(wmsParentalWrapper+"/shared/log/login-user", 
				bean, HttpMethod.POST, null);
		System.err.println("login : " + response.getBody());
	}
	
	public String resultSufficientAddNewUserEsbnCustom(UserBean userSid) {
		Map<String, String> map = new HashMap<String, String>();
		TokenWrapper tokenWrapper = getMyToken(ADDITION_USER, ADDITION_PASS,"visitor");
		map.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse result = wsBody(wmsParentalWrapper+"/registeration/save-url", userSid, HttpMethod.POST, map);
		System.err.println("hasil save : " + new Gson().toJson(result));
		Map<String, Object> mapp = mapperJsonToHashMap(result.getBody());
		if(((String)mapp.get("code")).equals("0")) {
			return (String) mapp.get("reason");
		}
		return (String) mapp.get("code");
	}
	
	public TblUserEsbnV2 getDetail(String userValidation,String password,int errorCode) {
		 TokenWrapper wrapper = getMyToken(userValidation, password, "user");
		
		 System.err.println("token wrappernya : " + new Gson().toJson(wrapper) + " jadi tokennya " + errorCode);
		
		 TblUserEsbnV2 esbnV2 = new TblUserEsbnV2(); 
		   if(wrapper.getReasonCode()==0) {
			   Map<String, String> map = new HashMap<String, String>();
			   map.put("Authorization", wrapper.getToken_type() + " " + wrapper.getAccess_token());
			   HttpRestResponse httpRestResponse = wsBody 
					   (wmsParentalWrapper+"/login-user/get-myown-user", null, 
					   HttpMethod.GET, 
					   map);
			  	try {
			  		TblUserEsbn esbn = new TblUserEsbn();
					esbn = mapperJsonToSingleDto(httpRestResponse.getBody(), TblUserEsbn.class);
				    esbn.setPassword(password);
				    esbnV2.setEsbn(esbn);
			  	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		   }
		   else{
			   esbnV2.setErrorCode(wrapper.getReasonCode());
		   }
		   return esbnV2;
	}
	
//	public Boolean isFirstLogin(String userSid) {
//		Boolean isFst = tblUserEsbnDao.isFirstLogin(userSid);
//		return isFst;
//	}
	
}
