package com.bank.mega.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.bank.mega.model.TblUserEsbn;
import com.google.gson.Gson;
public class CustomUserService implements UserDetails {

	private static final long serialVersionUID = -5446817407722834700L;
    private String userName;
    private String password;
    private boolean isActive = false;
  

	Collection<? extends GrantedAuthority> authorities;
    private String firstLogin = new Gson().toJson(new Date());
    private String longName;
    private String roleCode;
    private String roleDesc;
	
	public CustomUserService(TblUserEsbn tblUser) {
		//PasswordEncoder encoder = new BCryptPasswordEncoder();
		  this.userName = tblUser.getUserSid();
		  this.password = tblUser.getPassword();
	      isActive = tblUser.getIsActive();
	      this.roleCode = "user";
	      this.roleDesc = "Nasabah";
	      this.longName = tblUser.getUserName();
		  System.out.println("hasil : " + new Gson().toJson(tblUser));
		  List<GrantedAuthority> authorities = new ArrayList<>();
			  authorities.add(new SimpleGrantedAuthority(tblUser.getUserName()));
		  this.authorities = authorities;
	}
	
	
	
	  public String getFirstLogin() {
		return firstLogin;
	}



	public String getLongName() {
			return longName;
		}



		public String getRoleCode() {
			return roleCode;
		}



		public String getRoleDesc() {
			return roleDesc;
		}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return isActive;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
