package com.bank.mega;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.bank.mega.bean.EsbnEmailComplete;
import com.bank.mega.bean.EsbnPemesananEarlyRedemption;
import com.bank.mega.bean.EsbnPemesananTransaksi;
import com.bank.mega.bean.InjectorUserVariable;
import com.bank.mega.bean.TblEmailLogBean;
import com.bank.mega.bean.TblEsbnLogComponentWebService;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.encryptor.SecurityData;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.ParamQueryCustomLib;
import com.bank.mega.security.PrincipalSecurityData;
import com.bank.mega.security.WsResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.gson.Gson;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Component
public class UltimateBase {
	@Value("${esbn.version}")
	protected String ESBN_VERSION;

	@Value("${wms.parental.wrapper}")
	protected String wmsParentalWrapper;

	@Value("${wms.parental.user-uda}")
	protected String USER_UDA;

	@Value("${wms.parental.user-password}")
	protected String PASSWORD_UDA;

	@Value("${esbn.email-name}")
	protected String esbnEmailName;

	@Value("${esbn.email-password}")
	protected String esbnEmailPassword;

	@Value("${wms.parental.grant-type}")
	protected String GRANT_TYPE;

	@Value("${esbn.idStatus-complete}")
	protected String ESBN_IDSTATUS_COMPLETE;

	protected final String ADDITION_USER = "esbn-web";
	protected final String ADDITION_PASS = "TRUST-ME!CLIENT";
	protected final static String SUFFICIENT_SAVE = "1";
	protected final static String INSUFFICIENT_SAVE = "0";
	protected final static String STREAM_ESBN = "";
	protected final static String STREAM_WMS = "/registeration";
	protected final static String UNPARSEABLE = "Unperseable date";
    protected String REASON = "";
	protected MapperFacade mapperFacade = new DefaultMapperFactory.Builder().build().getMapperFacade();

	
	
	protected String changeFormatNumberMoney(Object obj) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		String format = nf.format(obj).replace("$", "");
		return format;
	}

	protected TokenWrapper getMyToken(String userName, String userPassword, String access) {
		InjectorUserVariable inject = new InjectorUserVariable();
		inject.setAccessType(access);
		inject.setPassword(userPassword);
		inject.setUser(userName);
		System.err.println("injector : " + new Gson().toJson(inject));
		try {
			System.err.println("encrpyt : " + new SecurityData().encrypt(new Gson().toJson(inject)));
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		Map<String, String> header = new HashMap<>();
		header.put("secret-method", "password");
		HttpRestResponse httpRestResponse = new HttpRestResponse();
		try {
			httpRestResponse = wsBody(wmsParentalWrapper + "/shared/oauth/token/v2/password-granter",
					new SecurityData().encrypt(new Gson().toJson(inject)), HttpMethod.POST, header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String, Object> mapToken = new HashMap<>();
		mapToken = mapResultApi(httpRestResponse.getBody());
		TokenWrapper tokenWrapper = new TokenWrapper();
		try {
			tokenWrapper = mapperHashmapToSingleDto(mapToken, TokenWrapper.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tokenWrapper;
	}

	protected PrincipalSecurityData getCredentialPrincipal(Authentication authentication) {
		String auth = new Gson().toJson(authentication.getPrincipal());
		PrincipalSecurityData securityData = new PrincipalSecurityData();
		try {
			securityData = mapperJsonToSingleDto(auth, PrincipalSecurityData.class);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return securityData;
	}
	
	protected TokenWrapper getMyToken(String bodyAuthentication) {
		Map<String, String> header = new HashMap<>();
		header.put("secret-method", "password");
		HttpRestResponse httpRestResponse = new HttpRestResponse();
		try {
			httpRestResponse = wsBody(wmsParentalWrapper + "/shared/oauth/token/v2/password-granter",
					bodyAuthentication, HttpMethod.POST, header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String, Object> mapToken = new HashMap<>();
		mapToken = mapResultApi(httpRestResponse.getBody());
		TokenWrapper tokenWrapper = new TokenWrapper();
		try {
			tokenWrapper = mapperHashmapToSingleDto(mapToken, TokenWrapper.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tokenWrapper;
	}

	protected TokenWrapper getMyToken(Authentication authentication) {
		String auth = new Gson().toJson(authentication.getPrincipal());
		PrincipalSecurityData securityData = new PrincipalSecurityData();
		try {
			securityData = mapperJsonToSingleDto(auth, PrincipalSecurityData.class);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InjectorUserVariable inject = new InjectorUserVariable();
		inject.setAccessType("USER");
		inject.setPassword(securityData.getPassword());
		inject.setUser(securityData.getUserName());
		Map<String, String> header = new HashMap<>();
		header.put("secret-method", "password");
		HttpRestResponse httpRestResponse = new HttpRestResponse();
		try {
			httpRestResponse = wsBody(wmsParentalWrapper + "/shared/oauth/token/v2/password-granter",
					new SecurityData().encrypt(new Gson().toJson(inject)), HttpMethod.POST, header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String, Object> mapToken = new HashMap<>();
		mapToken = mapResultApi(httpRestResponse.getBody());
		TokenWrapper tokenWrapper = new TokenWrapper();
		try {
			tokenWrapper = mapperHashmapToSingleDto(mapToken, TokenWrapper.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tokenWrapper;
	}

	protected static String parseDateToString(Date date, String format) {
		String dateFor = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		dateFor = sdf.format(date);
		return dateFor;
	}

	protected static String parseDateStringToAnotherStringFormat(String date, String oldFormat, String newFormat) {
		DateFormat originalFormat = new SimpleDateFormat(oldFormat);
		DateFormat targetFormat = new SimpleDateFormat(newFormat);
		Date dateFormat = null;
		try {
			dateFormat = originalFormat.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return UNPARSEABLE;
		}
		return targetFormat.format(dateFormat);
	}

	public boolean sendEmailRegistration(String to, String name, String sid,String sre, String dana) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p><b>Kepada Yth,</b></p>" + " <p><b>Bapak/Ibu " + name + " </b></p>"
				+ " </p></p>"
				+ " <p>Terima kasih atas kepercayaan anda untuk bergabung dengan layanan SBN Ritel Online Bank Mega. Registrasi telah berhasil dilakukan."
				+ " <p>" + " Berikut adalah data registrasi anda : "
				+ "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" + "<tr>"
				+ " <td width=\"40%\"> SID    </td>  " + " <td width=\"10%\"> :   </td>   " + " <td width=\"50%\">"
				+ sid + "</td>" + "</tr>"

				+ "<tr>" + " <td width=\"40%\">  No. Rekening Dana  </td>" + "  <td width=\"10%\">      :      </td>"
				+ " <td width=\"50%\">" +dana + "</td>" + "</tr>"

				+ " <tr>" + " <td width=\"40%\"> No. Rekening Efek </td>"
				+ "  <td width=\"10%\">       :    </td>  " + "<td width=\"50%\">" 
				+ sre+ "</td>" + "  </tr>"

				+ "</table>"
				+ " </p></p>" + " Silahkan lakukan pemesanan SBN Ritel melalui : sbnonline.bankmega.com " + " </div>"
                + " <p/><p/>"
                + " Terima kasih telah berpartisipasi dalam negeri."
		        +  " <p>PT Bank Mega, Tbk</p>" + " </div>";
		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Berhasil Registrasi SBN Ritel Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(sid);
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);
		
		return sendMailTest(to, "Konfirmasi Berhasil Registrasi SBN Ritel Bank Mega", bodyEmail, null, null);
	}
	
	public boolean sendEmailRegistrationFirst(String to, String name,String sid, String kodeVerifikasi, String password) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p><b>Kepada Yth,</b></p>" + " <p><b>Bapak/Ibu " + name + " </b></p>"
				+ " </p></p>"
                + "Registrasi Akun telah berhasil dilakukan. "
                + "Anda dapat melakukan log in ke e-SBN Ritel Online "
                + "dengan data SID dan Password yang sudah Anda daftarkan."
		+ "<p/>" + "<p/>" + "<p/>"
		+ " <p>Terima kasih telah berinvestasi dan berpartisipasi membangun negeri.</p>"
		+ " <p>PT Bank Mega, Tbk</p>" + " </div>";
		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		
		emailLogBean.setLogSubject("Konfirmasi Berhasil Registrasi e-SBN Ritel Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(sid);
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);
		
		return sendMailTest(to, "Konfirmasi Berhasil Registrasi e-SBN Ritel Bank Mega", bodyEmail, null, null);
	}
	
		

	public boolean sendEmailComplete(EsbnEmailComplete emailComplete) {
		System.out.println(" start send to " + emailComplete.getEmail());
		String parseDate =  parseDateStringToAnotherStringFormat(emailComplete.getWaktuPembayaran(), "yyyy-MM-dd'T'HH:mm:ss'Z'",  "dd MMM yyyy, HH:mm");
		if(parseDate.equalsIgnoreCase(UNPARSEABLE)) {
			REASON = "Parseable date from yyyy-MM-dd'T'HH:mm:ss'Z'";
			return false;
		}
		String bodyEmail = "<div> " + " <p><b>Kepada Yth,</b></p>" + " <p><b>Bapak/Ibu " + emailComplete.getNamaUser() + " </b></p>"
				+ " </p></p>"
			    + "Pembayaran Anda sudah kami terima dengan rincian pemesanan sebagai berikut :"
				+ "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" + "<tr>"
				+ " <td width=\"40%\"> Nama Seri Obligasi	    </td>  " + " <td width=\"10%\"> :   </td>   " + " <td width=\"50%\">"
				+ emailComplete.getNamaSeri() + "</td>" + "</tr>"

				+ "<tr>" + " <td width=\"40%\"> Kode Pemesanan  </td>" + "  <td width=\"10%\">      :      </td>"
				+ " <td width=\"50%\">" + emailComplete.getKodePemesanan() + "</td>" + "</tr>"

				+ " <tr>" + " <td width=\"40%\"> Nominal Pemesanan   </td>"
				+ "  <td width=\"10%\">       :    </td>  " + "<td width=\"50%\">" + "Rp "
				+ emailComplete.getNominalPemesanan() + "</td>" + "  </tr>"

				+ " <tr>"
				+ " <td width=\"40%\">Waktu Pembayaran </td>        <td width=\"10%\">  : </td>      "
				+ "<td width=\"50%\">" +parseDate+ "</td>" + " </tr>"

				+ " <tr>"
				+ " <td width=\"40%\"> <b>Kode NTPN</b>  </td>    <td width=\"10%\"><b>:</b> </td>     "
				+ " <td width=\"50%\"><b>" + emailComplete.getKodeNtpn() + "</b></td>" + " </tr>"

				+ "</table>"
				+ "<p/><p/><p/>"
				+ "<div>"
				+ "Terima kasih atas kepercayaan Anda bergabung dengan layanan SBN Ritel Online Bank Mega dan telah berpartisipasi membangun negeri."
				+ "</div>"
				+ " </p>" + " </p></p>" + " PT Bank Mega, Tbk" + " </div>";
		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Transaksi Sukses SBN Ritel Online Bank Mega ");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(emailComplete.getEmail());
		emailLogBean.setLogUser(emailComplete.getUserSid());
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);
		
		return sendMailTest(emailComplete.getEmail(), "Konfirmasi Transaksi Sukses SBN Ritel Online Bank Mega ", bodyEmail, null, null);
	}

	
	public boolean sendConfirmChangePhone(String to, String name, String phoneNew, String sid) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p><b>Kepada Yth,</b></p>" + " <p><b>Bapak/Ibu " + name + " </b></p>"
				+ " </p></p>"
				+ " <p>Kami menerima permintaan penggantian Nomor Telepon pada aplikasi e-SBN Online Anda menjadi "
				+ phoneNew + " pada aplikasi e-SBN Online Anda." + " <p>"
				+ " Jika bukan Anda yang melakukan permintaan tersebut, harap segera menghubungi Cabang Bank Mega terdekat atau hubungi MEGACALL di 60010 (HP) | 1500010 | +62 21 29601600 (from overseas only)."
				+ " </p>" + " <p>"
				+ " Terima kasih atas kepercayaan Anda bergabung dengan layanan SBN Ritel Online Bank Mega dan telah berpartisipasi membangun negeri."
				+ " </p>" + " </p></p>" + " PT Bank Mega, Tbk" + " </div>";

		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Perubahan Email e-SBN Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(sid);
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);

		return sendMailTest(to, "Konfirmasi Perubahan Nomor Telepon e-SBN Bank Mega", bodyEmail, null, null);
	}

	public boolean sendConfirmChangeEmail(String to, String name, String emailNew, String sid) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p><b>Kepada Yth,</b></p>" + " <p><b>Bapak/Ibu " + name + " </b></p>"
				+ " </p></p>"
				+ " <p>Kami menerima permintaan penggantian Alamat Email pada aplikasi e-SBN Online Anda menjadi "
				+ emailNew + " <p>"
				+ " Jika bukan Anda yang melakukan permintaan tersebut, harap segera menghubungi Cabang Bank Mega terdekat atau hubungi MEGACALL di 60010 (HP) | 1500010 | +62 21 29601600 (from overseas only)."
				+ " </p>" + " <p>"
				+ " Terima kasih atas kepercayaan Anda bergabung dengan layanan SBN Ritel Online Bank Mega dan telah berpartisipasi membangun negeri."
				+ " </p>" + " </p></p>" + " PT Bank Mega, Tbk" + " </div>";

		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Perubahan Email e-SBN Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(sid);
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);

		return sendMailTest(to, "Konfirmasi Perubahan Email e-SBN Bank Mega", bodyEmail, null, null);
	}

	public boolean sendConfirmationUserEmail(String to, String name, String sid, String password) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p>Kepada Yth, Bapak/Ibu " + name + " </p>" + " </p></p>"
				+ " <p>Terima kasih atas kepercayaan Anda Untuk bergabung dengan layanan SBN Ritel Online Bank Mega."
				+ " Berikut adalah konfirmasi untuk melakukan aktivitas akun Anda. " + " <p>" + " <b><small>"
				+ " Tanggal, Waktu :  " + parseDateToString(new Date(), "dd MMM yyyy, HH:mm") + " </small></b> </p>"
				+ " <p><b><small>" + " Username :  " + sid + " </small></b>  </p>" + " <p><b><small>"
				+ " Kode Verifikasi : " + password + " </small></b> </p>" + " </p> " + " <p>"
				+ " Catatan : Masa berlaku OTP hanya 1 menit, dengan 3 kali kesalahan pengisian. </small></b> </p>" + " </p> " + " <p>"
				+ " Silahkan melakukan aktivasi akun Anda dengan cara melakukan login pada SBN Ritel Online Bank Mega "
				+ " menggunakan username dan password di atas. Untuk keamanan Anda segera ubah Password Anda setelah melakukan login pertama kali. "
				+ " </p>"
				+ " <p>Mohon tidak memberikan Username dan Password Anda kepihak manapun. Segala risiko yang timbul akibat penyalahgunaan"
				+ " Username dan Password bukan merupakan tanggung jawab PT Bank Mega, Tbk.</p>" + " </p> "
				+ " <p>Terima kasih</p>" + " PT Bank Mega, Tbk" + " </div>";

		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Password Login e-SBN Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(sid);
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);

		return sendMailTest(to, "Konfirmasi Password Login e-SBN Bank Mega", bodyEmail, null, null);
	}

	public boolean sendTransaksiPemesananBilling(String to, String name,
			EsbnPemesananTransaksi esbnPemesananTransaksi) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p><h4><b>Kepada Yth, <p/>" + " Bapak/Ibu " + name + " </h4></b></p>"
				+ " </p></p>"
				+ " <p>Pemesanan SBN Ritel melalui Bank Mega berhasil. Berikut adalah konfirmasi transaksi Pemesanan Anda : "

				+ "<p/>" + "<p/>" + "<p/>" 
				+ "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" + "<tr>"
				+ " <td width=\"40%\"> 1.SID    </td>  " + " <td width=\"10%\"> :   </td>   " + " <td width=\"50%\">"
				+ esbnPemesananTransaksi.getSid() + "</td>" + "</tr>"

				+ "<tr>" + " <td width=\"40%\"> 2.Nama Seri Obligasi  </td>" + "  <td width=\"10%\">      :      </td>"
				+ " <td width=\"50%\">" + esbnPemesananTransaksi.getSeri() + "</td>" + "</tr>"

				+ "<tr>" + "<td width=\"40%\"> 3.Kode Pemesanan </td>    " + "<td width=\"10%\">         :   </td>   "
				+ "<td width=\"50%\">" + esbnPemesananTransaksi.getKodePemesanan() + "</td>" + " </tr>"

				+ " <tr>" + " <td width=\"40%\"> 4.Nominal Pemesanan   </td>"
				+ "  <td width=\"10%\">       :    </td>  " + "<td width=\"50%\">" + "IDR "
				+ changeFormatNumberMoney(esbnPemesananTransaksi.getNominal()) + "</td>" + "  </tr>"

				+ " <tr>"
				+ " <td width=\"40%\"><b>5.Kode Billing</b>  </td>        <td width=\"10%\">      <b>:</b></td>      "
				+ "<td width=\"50%\"><b>" + esbnPemesananTransaksi.getKodeBilling() + "</b></td>" + " </tr>"

				+ " <tr>"
				+ " <td width=\"40%\"> <b>6.Batas Waktu Pembayaran</b>  </td>    <td width=\"10%\"><b>:</b> </td>     "
				+ " <td width=\"50%\"><b>" + esbnPemesananTransaksi.getBatasWaktuBayar() + "</b></td>" + " </tr>"

				+ "</table>" + "<p/>" + "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" >"
				+ "<td width=\"1%\" valign=\"top\"> <b>Catatan </b> </td>"
				+ "<td width=\"1%\" valign=\"top\"> <b>:</td>"
				+ " <td width=\"30%\" valign=\"top\" style=\"text-align:justify;\"> <b> Segera lakukan pembayaran sebelum Batas Waktu Pembayaran. "
				+ "  Jika tidak dilakukan pembayaran, maka pemesanan secara otomatis batal/menjadi "
				+ "  kadaluarsa dan kuota akan kembali setelah 2 hari kerja. </b> " + "</td></table>"
				+"<p/><p/><p/>"
				+"<div style=\"text-align:justify;\">"
				+ "<h3>Cara Pembayaran Melalui Kantor Cabang</h3>"
				+ "<p/>" + "<p/>" + "<p/>"
				+"Pembayaran dilakukan melalui Teller Kantor Cabang Bank Mega dengan membawa buku tabungan atau kartu debit serta kode billing pemesanan surat berharga dengan tujuan pembayaran Pajak/MPN menggunakan kode biller 50012."   
				+ "<p/><p/><p/>"
				+"Tidak ada Limit Transaksi harian jika Pembayaran dilakukan melalui Teller Kantor Cabang."
				+"</div>"
				+ "<p/>" + "<p/>" + "<p/>"
				+ "<h3>Cara Pembayaran Melalui ATM Bank Mega</h3>"
				+ "<p/>" + "<p/>" + "<p/>"
				+ " <p>Tata cara pembayaran :</p>"
				+"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"height: 275px;\">"
				+"<tr>"
				+"<td width=\"20px\" valign=\"top\">1. </td>"
				+"<td>Masukkan Kartu Debit, kemudian pilih Bahasa Indonesia</td>"
				+"</tr>"
				+"<tr>"
				+"<td width=\"10px\" valign=\"top\">2. </td>"
				+"<td>Ketik Nomor PIN Kartu Debit kemudian tekan ENTER</td>"
				+"</tr>"
				+"<tr>"
				+"<td   width=\"10px\" valign=\"top\">3. </td>"
				+"<td>Pilih menu DONASI CT ARSA/PEMBAYARAN.</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">4. </td>"
				+"<td>Pilih DONASI CT ARSA/LAINNYA</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">5. </td>"
				+"<td>Pilih PAJAK/MPN</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">6. </td>"
				+"<td>Pilih e-SBN RETAIL</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">7. </td>"
				+"<td>Masukkan KODE BILLING</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">8. </td>"
				+"<td>Konfirmasi data Kode Billing.</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">9. </td>"
				+"<td>Pilih YA untuk melanjutkan transaksi bila data sesuai</td>"
				+"</tr>"
				+"<tr>"
				+"<td  width=\"10px\" valign=\"top\">10. </td>"
				+"<td>Bukti Transaksi akan tercetak dalam bentuk struk ATM Bank Mega</td>"
				+"</tr>"
				+"</table>"
				+"<p/><p/><p/><p/>"

				+"Terdapat ketentuan maksimum pembayaran melalui channel ATM Bank Mega sesuai dengan jenis kartu nasabah pada tabel dibawah ini:"
				+"<p/><p/><p/>"
				
				+"<div style=\"margin-left=10px;\">"
				+"<table cellspacing=\"0\" cellpadding=\"0\" border=\"1px solid #ddd\" style=\"height: 80px;\" width=\"90%\">"

				+"<tr>"
				+"<td   align=\"center\"><b>&nbsp; &nbsp; Limit Transaksi Harian &nbsp; &nbsp; </b></td>"
				+"<td      align=\"center\"><b>&nbsp;&nbsp;Classic&nbsp;&nbsp;</b></td>"
				+"<td    align=\"center\"><b>&nbsp;&nbsp;Preferred&nbsp;&nbsp;</b></td>"
				+"<td    align=\"center\"><b>&nbsp;&nbsp;Mega First&nbsp;&nbsp;</b></td>"
				+"</tr>"

				+"<tr>"
				+"<td align=\"left\">Pembayaran </td>"
				+"<td   align=\"left\">500 juta</td>"
				+"<td   align=\"left\">1 Miliyar</td>"
				+"<td   align=\"left\">2 Miliyar</td>"
				+"</tr>"
				+"</table>"
				+"</div>"
				+"<p/><p/><p/>"
				+ "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n" + 
				"<tr>\n" + 
				"<td valign=\"top\">\n" + 
				"<b>*</b>\n" + 
				"</td>\n" + 
				"\n" + 
				"\n" + 
				"<td style=\"text-align:justify;\">\n" + 
				"<b>Jika nominal pemesanan melebihi limit transaksi harian pembayaran, Nasabah dapat melakukan pembayaran melalui teller kantor cabang Bank Mega atau melakukan pembayaran menggunakan ATM Bank Mega dengan membagi beberapa pemesanan di hari yang berbeda dan menyesuaikan Limit Transaksi harian.\n" + 
				"</b>\n" + 
				"</td>\n" + 
				"</tr>\n" + 
				"</table>"
				+ "<p/>" + " <p>Terima kasih telah berinvestasi dan berpartisipasi membangun negeri.</p>"
				+ " <p>PT Bank Mega, Tbk</p>" + " </div>";

		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Kode Billing Pemesanan e-SBN Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(esbnPemesananTransaksi.getSid());
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);

		return sendMailTest(to, "Konfirmasi Kode Billing Pemesanan e-SBN Bank Mega", bodyEmail, null, null);
	}

	public boolean sendTransaksiEarlyRedemptionBilling(String to, String name,
			EsbnPemesananEarlyRedemption esbnPemesananEarlyRedemption, 
			String rekeningDana, String reason) {
		System.out.println(" start send to " + to);
		String bodyEmail = "<div> " + " <p><h4><b>Kepada Yth, <p/>" + " Bapak/Ibu " + name + " </h4></b></p>"
				+ " </p></p>"
				+ " <p>Terima kasih atas kepercayaan Anda Untuk bergabung dengan layanan SBN Ritel Online Bank Mega."
				+ " Berikut Adalah Konfirmasi Transaksi <i>Redemption</i> Anda : "

				+ "<p/>" + "<p/>" + "<p/>"

				+ "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" + " <tr>"
				+ " <td width=\"40%\">1.SID </td>                                " + "<td width=\"10%\">  :   </td>  "
				+ " <td width=\"50%\">" + esbnPemesananEarlyRedemption.getSid() + "</td>" + " </tr>"

				+ " <tr>" + " <td width=\"40%\">2.Nama Seri Obligasi </td>              "
				+ "<td width=\"10%\">     : </td>     " + "<td width=\"50%\">" + esbnPemesananEarlyRedemption.getSeri()
				+ "</td>" + " </tr>"

				+ " <tr>" + " <td width=\"40%\">" + " 3.Kode <i>Redeem</i> </td>  "
				+ "<td width=\"10%\">                    :  </td>" + " <td width=\"50%\">    "
				+ esbnPemesananEarlyRedemption.getKodePemesanan() + "</td>" + " </tr>"

				+ " <tr>" + " <td width=\"40%\">4.Nominal <i>Redeem</i></td>"
				+ "<td width=\"10%\">                    : </td>" + " <td width=\"50%\">     " + "IDR "
				+ changeFormatNumberMoney(esbnPemesananEarlyRedemption.getNominal()) + " </td></tr>"

				+ " <tr>"

				+ "<td width=\"40%\"><b>5.Sisa Nominal Yang Dapat di <i>Redeem</i> </b> </td>"
				+ "<td width=\"10%\">   <b>:</b>  </td>    " + "<td width=\"50%\"><b>IDR</b> <b>"
				+ changeFormatNumberMoney(esbnPemesananEarlyRedemption.getRedeemable()) + "</b></td>" + " </tr>"

				+ " <tr>"
				+ " <td width=\"40%\"> <b>6.Sisa Kepemilikan</b></td>               <td width=\"10%\">       <b> : </b>   </td>  "
				+ "<td width=\"50%\"><b>IDR "
				+ changeFormatNumberMoney(esbnPemesananEarlyRedemption.getSisaKepemilikan()) + "</b></td>" + " </tr>"

				+ " <tr>" + "  <td width=\"40%\"> <b>7.Rekening Pengkreditan</b>  </td>         "
				+ "<td width=\"10%\">      <b>:</b>    </td>  " + " <td width=\"50%\"><b>" + rekeningDana + "</b></td>"
				+ " </tr>"

				+ "</table>"
                + "<p/><p/><p/>"
				+ reason
				+ "<p/>" + "<p/>" + "<p/>"
				+ " <p>Terima kasih telah berinvestasi dan berpartisipasi membangun negeri.</p>"
				+ " <p>PT Bank Mega, Tbk</p>" + " </div>";

		TblEmailLogBean emailLogBean = new TblEmailLogBean();
		Map<String, String> headerMap = new HashMap<>();
		emailLogBean.setLogCc(" ");
		emailLogBean.setLogResend(" ");
		emailLogBean.setLogSubject("Konfirmasi Early Redemption e-SBN Bank Mega");
		emailLogBean.setLogTglWaktu(new Date());
		emailLogBean.setLogTujuan(to);
		emailLogBean.setLogUser(esbnPemesananEarlyRedemption.getSid());
		HttpRestResponse responseEmailLog = wsBody(wmsParentalWrapper + "/shared/save/email-log", emailLogBean,
				HttpMethod.POST, headerMap);

		return sendMailTest(to, "Konfirmasi Early Redemption e-SBN Bank Mega", bodyEmail, null, null);
	}

	public boolean sendMailTest(String to, String subject, String body, String attachment,
			Map<String, String> mapInlineImages) {
        //e-SBN@bankmega.com
		String username = esbnEmailName;
		// final String username_from = "digirec.ifwd@fwd.co.id";
		//Bankmega7*
		String password = esbnEmailPassword;
		System.err.println("pass : " + password + " user : " + username);
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.smtp.quitwait", "false");
		props.setProperty("mail.host", "smtp.bankmega.com");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.bankmega.com");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

//		    message.setFrom(new InternetAddress(username)); 
//		      
//		    message.addRecipient(Message.RecipientType.TO,  
//		                          new InternetAddress(to)); 
//		    message.setSubject("Verifikasi Informasi User"); 
//		    message.setText("Hi, I'm Sending From Java"); 
//		  
//		    // Send message 
//		    Transport.send(message); 
//		    System.out.println("Yo it has been sent.."); 

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);

			msg.setFrom(new InternetAddress(username));
			// msg.setFrom(new InternetAddress(username_from));
			InternetAddress[] toAddresses = { new InternetAddress(to) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/html");

			// creates multi-part
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			if (mapInlineImages != null && mapInlineImages.size() > 0) {
				Set<String> setImageID = mapInlineImages.keySet();

				for (String contentId : setImageID) {
					MimeBodyPart imagePart = new MimeBodyPart();
					imagePart.setHeader("Content-ID", "<" + contentId + ">");
					imagePart.setDisposition(MimeBodyPart.INLINE);

					String imageFilePath = mapInlineImages.get(contentId);
					try {
						imagePart.attachFile(imageFilePath);
					} catch (IOException ex) {
						ex.printStackTrace();
					}

					multipart.addBodyPart(imagePart);
				}
			}

			msg.setContent(multipart);
			System.err.println("masuk kesini");

			Transport.send(msg);

			System.out.println("Done");

			return true;
		} catch (MessagingException e) {
			REASON = e.getMessage() + " ";
			e.printStackTrace();
			return false;
		}
	}

	protected String randomString(int leng) {
		int leftLimit = 97; // letter 'a'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = leng;
		Random random = new Random();
		StringBuilder buffer = new StringBuilder(targetStringLength);
		for (int i = 0; i < targetStringLength; i++) {
			int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
			buffer.append((char) randomLimitedInt);
		}
		String generatedString = buffer.toString();

		return generatedString;
	}

	protected HttpRestResponse wsBodyWms(String secretUrl, String secretMethod, Object body,
			Map<String, String> headerMap, ParamQueryCustomLib... paramQuery) {
		if (body == null) {
			body = new HashMap<>();
		}
		if (headerMap == null) {
			headerMap = new HashMap<>();
		}
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		HttpRestResponse result = wsBody(wmsParentalWrapper + STREAM_WMS + "/WMS-URL", body, HttpMethod.POST,
				headerMap);
		return result;
	}

	protected HttpRestResponse wsBodyWms(String secretUrl, String secretMethod, Object body,
			Map<String, String> headerMap, String authorization, ParamQueryCustomLib... paramQuery) {
		if (body == null) {
			body = new HashMap<>();
		}
		if (headerMap == null) {
			headerMap = new HashMap<>();
		}
		headerMap.put("Authorization", authorization);
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		System.out.println("my header : " + new Gson().toJson(headerMap));
		HttpRestResponse result = wsBody(wmsParentalWrapper + STREAM_WMS + "/WMS-URL", body, HttpMethod.POST,
				headerMap);
		return result;
	}

	protected HttpRestResponse wsBodyEsbnWithoutBody(String secretUrl, String secretMethod,
			Map<String, String> headerMap, String authorization, ParamQueryCustomLib... paramQuery) {

		if (headerMap == null) {
			headerMap = new HashMap<>();
		}
		headerMap.put("Authorization", authorization);
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		System.out.println("my header : " + new Gson().toJson(headerMap));
		HttpRestResponse result = wsBody(wmsParentalWrapper + STREAM_ESBN + "/esbn-connector/call-url/no-body", null,
				HttpMethod.GET, headerMap);
		return result;
	}

	protected HttpRestResponse wsBodyEsbnWithBody(String secretUrl, String secretMethod, Object body,
			Map<String, String> headerMap, String authorization, ParamQueryCustomLib... paramQuery) {

		if (headerMap == null) {
			headerMap = new HashMap<>();
		}
		headerMap.put("Authorization", authorization);
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		System.out.println("my header : " + new Gson().toJson(headerMap));
		HttpRestResponse result = wsBody(wmsParentalWrapper + STREAM_ESBN + "/esbn-connector/call-url/with-body", body,
				HttpMethod.POST, headerMap);
		return result;
	}

	protected WsResponse getResultWsWms(String secretUrl, String secretMethod, Object body,
			Map<String, String> headerMap, ParamQueryCustomLib... paramQuery) {
		WsResponse wsResponse = new WsResponse();
		if (body == null) {
			body = new HashMap<>();
		}
		HttpRestResponse httpRestResponse = wsBodyWms(secretUrl, secretMethod, body, headerMap, paramQuery);

		switch (httpRestResponse.getStatus()) {
		case OK:
			ObjectMapper om = new ObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			try {
				wsResponse = om.readValue(httpRestResponse.getBody(), WsResponse.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return wsResponse;
		case NOT_ACCEPTABLE:
			return null;
		case INTERNAL_SERVER_ERROR:
			return null;
		default:
			return null;
		}
	}

	protected WsResponse getResultWs(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		WsResponse wsResponse = new WsResponse();
		HttpRestResponse httpRestResponse = wsBody(url, body, method, headerMap, paramQuery);

		switch (httpRestResponse.getStatus()) {
		case OK:
			ObjectMapper om = new ObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			try {
				wsResponse = om.readValue(httpRestResponse.getBody(), WsResponse.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return wsResponse;
		case NOT_ACCEPTABLE:
			return null;
		case INTERNAL_SERVER_ERROR:
			return null;
		default:
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	protected HttpRestResponse wsBody(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}
		StringBuilder paramBuilder = new StringBuilder();
		if (paramQuery != null) {
			if (paramQuery.length != 0) {
				paramBuilder.append("?");
				for (int i = 0; i < paramQuery.length; i++) {
					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
					if (i < paramQuery.length - 1) {
						paramBuilder.append("&");
					}
				}
			}
		}
		System.err.println("body : " + new Gson().toJson(body));
		System.err.println("header : " + new Gson().toJson(headers));

		HttpEntity httpEntity = new HttpEntity(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));

		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(url.concat(paramBuilder.toString()), method,
					httpEntity, String.class);
			System.err.println("status : " + responseEntity.getStatusCode());
			System.err.println("result api : " + responseEntity.getBody());
			TblEsbnLogComponentWebService comp = new TblEsbnLogComponentWebService();
			comp.setBody(new Gson().toJson(body));
			comp.setHeader(new Gson().toJson(headers));
			comp.setMethod(new Gson().toJson(method));
			comp.setResultShow(new Gson().toJson( new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody())));
			comp.setUrl(url);
			comp.setUserId("NO-AUTH");
			
			if(headerMap!=null && headerMap.get("Authorization")!=null) {
				System.err.println("masuk kesini kagak broo");
				Map<String, String> headerMapToken = new HashMap<>();
				headerMapToken.put("Authorization", headerMap.get("Authorization"));
 				HttpRestResponse responseToken = wsBodyToken
						(wmsParentalWrapper+"/userManagementCtl/whose-token-is-this", 
								null, HttpMethod.GET, headerMapToken);
 				comp.setUserId(responseToken.getBody());
			}
			
			if(!url.contains("shared/oauth/token/v2/password-granter")) {
			wsBodyLogging(wmsParentalWrapper+"/shared/save/webservice-log", 
					comp, HttpMethod.POST, null);
			}
			return new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (Exception exp) {
			if (exp.getMessage().contains("400")) {
				System.err.println("error 400");
				TblEsbnLogComponentWebService comp = new TblEsbnLogComponentWebService();
				comp.setBody(new Gson().toJson(body));
				comp.setHeader(new Gson().toJson(headers));
				comp.setMethod(new Gson().toJson(method));
				comp.setResultShow(new Gson().toJson(new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah")));
				comp.setUrl(url);
				comp.setUserId("NO-AUTH");
				if(!url.contains("shared/oauth/token/v2/password-granter")) {
				wsBodyLogging(wmsParentalWrapper+"/shared/save/webservice-log", 
						comp, HttpMethod.POST, null);
				}
				return new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah");
			} else if (exp.getMessage().contains("500")) {
				System.err.println("error 500");
				TblEsbnLogComponentWebService comp = new TblEsbnLogComponentWebService();
				comp.setBody(new Gson().toJson(body));
				comp.setHeader(new Gson().toJson(headers));
				comp.setMethod(new Gson().toJson(method));
				comp.setResultShow(new Gson().toJson( new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error")));
				comp.setUrl(url);
				comp.setUserId("NO-AUTH");
				if(!url.contains("shared/oauth/token/v2/password-granter")) {
				wsBodyLogging(wmsParentalWrapper+"/shared/save/webservice-log", 
						comp, HttpMethod.POST, null);
				}
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
			} else if (exp.getMessage().contains("Connection refused")) {
				System.err.println("Connection refused");
				TblEsbnLogComponentWebService comp = new TblEsbnLogComponentWebService();
				comp.setBody(new Gson().toJson(body));
				comp.setHeader(new Gson().toJson(headers));
				comp.setMethod(new Gson().toJson(method));
				comp.setResultShow(new Gson().toJson( new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service")));
				comp.setUrl(url);
				comp.setUserId("NO-AUTH");
				if(!url.contains("shared/oauth/token/v2/password-granter")) {
				wsBodyLogging(wmsParentalWrapper+"/shared/save/webservice-log", 
						comp, HttpMethod.POST, null);
				}
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service");
			} else {
				System.err.println("exp said: " + exp.getMessage());
				System.err.println("error unidentified");
				TblEsbnLogComponentWebService comp = new TblEsbnLogComponentWebService();
				comp.setBody(new Gson().toJson(body));
				comp.setHeader(new Gson().toJson(headers));
				comp.setMethod(new Gson().toJson(method));
				comp.setResultShow(new Gson().toJson( new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record")));
				comp.setUrl(url);
				comp.setUserId("NO-AUTH");
				wsBodyLogging(wmsParentalWrapper+"/shared/save/webservice-log", 
						comp, HttpMethod.POST, null);
				return new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record");
			}
		}
	}

	@SuppressWarnings("rawtypes")
	protected HttpRestResponse wsBodyToken(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}
		StringBuilder paramBuilder = new StringBuilder();
		if (paramQuery != null) {
			if (paramQuery.length != 0) {
				paramBuilder.append("?");
				for (int i = 0; i < paramQuery.length; i++) {
					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
					if (i < paramQuery.length - 1) {
						paramBuilder.append("&");
					}
				}
			}
		}
		System.err.println("body : " + new Gson().toJson(body));
		System.err.println("header : " + new Gson().toJson(headers));

		HttpEntity httpEntity = new HttpEntity(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));

		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(url.concat(paramBuilder.toString()), method,
					httpEntity, String.class);
			System.err.println("status : " + responseEntity.getStatusCode());
			System.err.println("result api : " + responseEntity.getBody());
			return new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (Exception exp) {
			if (exp.getMessage().contains("400")) {
				System.err.println("error 400");
				return new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah");
			} else if (exp.getMessage().contains("500")) {
				System.err.println("error 500");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
			} else if (exp.getMessage().contains("Connection refused")) {
				System.err.println("Connection refused");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service");
			} else {
				System.err.println("exp said: " + exp.getMessage());
				System.err.println("error unidentified");
				return new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record");
			}
		}
	}
	

	@SuppressWarnings("rawtypes")
	protected HttpRestResponse wsBodyLogging(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}
		StringBuilder paramBuilder = new StringBuilder();
		if (paramQuery != null) {
			if (paramQuery.length != 0) {
				paramBuilder.append("?");
				for (int i = 0; i < paramQuery.length; i++) {
					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
					if (i < paramQuery.length - 1) {
						paramBuilder.append("&");
					}
				}
			}
		}
		System.err.println("body : " + new Gson().toJson(body));
		System.err.println("header : " + new Gson().toJson(headers));

		HttpEntity httpEntity = new HttpEntity(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));

		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(url.concat(paramBuilder.toString()), method,
					httpEntity, String.class);
			System.err.println("status : " + responseEntity.getStatusCode());
			System.err.println("result api : " + responseEntity.getBody());
			return new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (Exception exp) {
			if (exp.getMessage().contains("400")) {
				System.err.println("error 400");
				return new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah");
			} else if (exp.getMessage().contains("500")) {
				System.err.println("error 500");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
			} else if (exp.getMessage().contains("Connection refused")) {
				System.err.println("Connection refused");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service");
			} else {
				System.err.println("exp said: " + exp.getMessage());
				System.err.println("error unidentified");
				return new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record");
			}
		}
	}
	
	
	
	private Map<String, Object> mapResultApi(String result) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String, Object> finalMap = new HashMap<>();
		try {
			finalMap = mapper.readValue(result, new TypeReference<HashMap<String, Object>>() {
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finalMap;
	}

	protected Map<String, Object> mapperJsonToHashMap(String result) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String, Object> finalMap = new HashMap<>();
		try {
			finalMap = mapper.readValue(result, new TypeReference<HashMap<String, Object>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
			finalMap.put("error_method", e.getMessage());
		}
		return finalMap;
	}

	protected String setObjectToString(Object object) {
		return new Gson().toJson(object);
	}

	protected <T> T mapperJsonToSingleDto(String json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return om.readValue(json, clazz);
	}

	protected <T> T mapperHashmapToSingleDto(Map<String, Object> json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return om.convertValue(json, clazz);
	}

	protected <T> List<T> mapperJsonToListDto(String json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		TypeFactory t = TypeFactory.defaultInstance();
		List<T> list = om.readValue(json, t.constructCollectionType(ArrayList.class, clazz));
		return list;
	}

}
