# PLAIN TEMPLATE RUN SPRING-BOOT IN TOMCAT 
---


characteristics
---

* Spring Boot;
* Spring Security for basic login with permissions;
* Thymeleaf as view Template Engine;
* Postgres as Database or others;
* Basic customer CRUD;


This is database

| #   | Username | Password |
| --- |:--------:| --------:|
| 1   | sora    | 1234     |
| 2   | roxas   | 1234     |

